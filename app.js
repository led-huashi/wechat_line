//app.js
var onenet = require('js/onenet.js');

App({
  onLaunch: function () {
    // 展示本地存储能力
    var that = this;
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    
    //get devices of onenet
    wx.request({
      url: "https://api.heclouds.com/devices",
      header: {
        "api-key": "eCQJZEKoyVqA5qV4ef3qTH2OZzo="
      },
      data: {

      },
      success(res) {
        console.log(res)
        var devices = [];
        if (res.data.errno == 0) {
          for (var i = 0; i < res.data.data.total_count; i++) {
            devices.push(res.data.data.devices[i]);
          }
        }
        that.globalData.devices = devices
        console.log(devices)
      },
      fail(res) {
        console.log("请求失败")
        // deviceConnected = false
      }
    })
  },
  //全局变量
  globalData: {
    userInfo: null,
    devices: [],    //获取的设备列表
    equimentList:[
      {id:1,title:"LED1",img:"../../images/led/led.jpg"},
      {id:2,title:"LED2",img:"../../image/led/led.jpg"},
    ],     //设置的设备列表
    SystemInfo: {}  //蓝牙
  },
  //蓝牙部分
  buf2hex: function (buffer) {
    return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('')
  },
  buf2string: function (buffer) {
    var arr = Array.prototype.map.call(new Uint8Array(buffer), x => x)
    var str = ''
    for (var i = 0; i < arr.length; i++) {
      str += String.fromCharCode(arr[i])
    }
    return str
  },
  onLaunch: function () {
    this.globalData.SystemInfo = wx.getSystemInfoSync()
    //console.log(this.globalData.SystemInfo)
  },
})