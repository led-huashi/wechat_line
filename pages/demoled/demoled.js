// var WxAutoImage = require('../../js/wxAutoImageCal.js');
// var onenet = require('../../js/onenet.js');
// var API_KEY = "eCQJZEKoyVqA5qV4ef3qTH2OZzo="
var app = getApp();

Page({
  data: {
    showModal: false,
    imgList: [
      {
        "id": 0,
        "title": "白光",
        "img": '../../image/CCT_black.png',
        "imgBlue": "../../image/CCT_blue.png"
      },
      // {
      //   "title": "全彩",
      //   "img": '../../image/RGBImg.png'
      // },
      {
        "id": 1,
        "title": "HSI",
        "img": '../../image/HSI_black.png',
        "imgBlue": "../../image/HSI_blue.png"
      },
      {
        "id": 2,
        "title": "爆炸",
        "img": '../../image/explosion_black.png',
        "imgBlue": "../../image/explosion_blue.png"
      },
      {
        "id": 3,
        "title": "迪厅",
        "img": '../../image/disco_black.png',
        "imgBlue": "../../image/disco_blue.png"
      },
      {
        "id": 4,
        "title": "电焊",
        "img": '../../image/welding_black.png',
        "imgBlue": "../../image/welding_blue.png"
      },
      {
        "id": 5,
        "title": "TV",
        "img": '../../image/TV_black.png',
        "imgBlue": "../../image/TV_BLUE.png"
      },
      {
        "id": 6,
        "title": "短路",
        "img": '../../image/shorts_black.png',
        "imgBlue": "../../image/shorts_blue.png"
      },
      {
        "id": 7,
        "title": "狗仔",
        "img": '../../image/paparazzi_black.png',
        "imgBlue": "../../image/paparazzi_blue.png"
      },
      {
        "id": 8,
        "title": "呼吸",
        "img": '../../image/breathe_black.png',
        "imgBlue": "../../image/breathe_blue.png"
      },
      {
        "id": 9,
        "title": "火焰",
        "img": '../../image/flame_black.png',
        "imgBlue": "../../image/flame_blue.png"
      },
      {
        "id": 10,
        "title": "警灯",
        "img": '../../image/policeLight_black.png',
        "imgBlue": "../../image/policeLight_blue.png"
      },
      {
        "id": 11,
        "title": "蜡烛",
        "img": '../../image/candles_black.png',
        "imgBlue": "../../image/candles_blue.png"
      },
      {
        "id": 12,
        "title": "流光",
        "img": '../../image/color_black.png',
        "imgBlue": "../../image/color_blue.png"
      },
      {
        "id": 13,
        "title": "派对",
        "img": '../../image/party_black.png',
        "imgBlue": "../../image/party_blue.png"
      },
      {
        "id": 14,
        "title": "频闪",
        "img": '../../image/strobe_black.png',
        "imgBlue": "../../image/strobe_blue.png"
      },
      {
        "id": 15,
        "title": "闪电",
        "img": '../../image/lightning_black.png',
        "imgBlue": "../../image/lightning_blue.png"
      },
      {
        "id": 16,
        "title": "循环",
        "img": '../../image/circulation_black.png',
        "imgBlue": "../../image/circulation_blue.png"
      },

      {
        "id": 17,
        "title": "烟花",
        "img": '../../image/fireworks_black.png',
        "imgBlue": "../../image/fireworks_blue.png"
      },
      {
        "id": 18,
        "title": "移动",
        "img": '../../image/movement_black.png',
        "imgBlue": "../../image/movement_blue.png"
      },
      {
        "id": 19,
        "title": "音乐",
        "img": '../../image/music_black.png',
        "imgBlue": "../../image/music_blue.png"
      },
      {
        "id": 20,
        "title": "云飘过",
        "img": '../../image/cloudsFloating_black.png',
        "imgBlue": "../../image/cloudsFloating_blue.png"
      }
    ],

    // imgList1: [
    //   {
    //     "title":"闪电",
    //     "img": "../../image/lighting.png"
    //   },
    //     {
    //       "title":"爆炸",
    //       "img": "../../image/boom.png"
    //     },
    //     {
    //       "title":"警车",
    //       "img": "../../image/Fire_truck.png"
    //     },
    //     {
    //       "title":"警车1",
    //       "img": "../../image/Fire_truck.png"
    //     },
    //     {
    //       "title":"电视",
    //       "img": "../../image/TV.png"
    //     },
    //     {
    //       "title":"灯光",
    //       "img": "../../image/paparazzo.png"
    //     },
    //     {
    //       "title":"火花1",
    //       "img": "../../image/fire.png"
    //     },
    //     {
    //       "title":"焊接",
    //       "img": "../../image/welding.png"
    //     },
    // ],

    // 通道
    optionList:[
      {
        "id": 0,
        "name": '通道1',
      },
      {
        "id": 1,
        "name": '通道2',
      },
      {
        "id": 2,
        "name": '通道3',
      },
      {
        "id": 3,
        "name": '通道4',
      },
      {
        "id": 4,
        "name": '通道5',
      },
      {
        "id": 5,
        "name": '通道6',
      },
      {
        "id": 6,
        "name": '通道7',
      },
      {
        "id": 7,
        "name": '通道8',
      },
    ],
    
    ch: 0,
    curTab: 0,
    equipment_name:"",
    input_name:"",
    id:0,
    redSwitchChecked:false,
    greenSwitchChecked:false,
    blueSwitchChecked:false,
    switchFlag:false,
    myintervalid:0,

    // 通道
    channel: "通道1",
    bright: 50,       //亮度
    // brightHex: "32",    // 亮度的十六进制
    colorTemp: 5400,    //色温
    r: 128,
    g: 128,
    b: 128,
    f:1,              //频率
    //光效的可调节参数
    fire_bright:50,
    fire_saturation:50,
    fire_f:1,
    firework_bright:50,
    firewoek_color:50,
    firework_f:1,
    cloud_bright:50,
    cloud_v:50,
    currentIndex1: 0,
    
    // 弹窗
    hideFlag: true,   //true-隐藏  false-显示
    animationData: {},    //
  },

  
// Tab部分
  changeTab: function(e) {
    let curTab = e.currentTarget.dataset.curtab;
    this.setData({
      curTab
    })
  },

//Sliders部分
changefire_bright(e){
  this.setData({ 
    fire_bright: e.detail.value,
  })
},
changefire_saturation(e){
  this.setData({ 
    fire_saturation:e.detail.value,
  })
},
changefire_f(e){
  this.setData({ 
    fire_f:e.detail.value,
  })
},
changefirework_bright(e){
  this.setData({ 
    firework_bright: e.detail.value,
  })
},
changefirework_color(e){
  this.setData({ 
    firework_color:e.detail.value,
  })
},
changefirework_f(e){
  this.setData({ 
    firework_f:e.detail.value,
  })
},
changecloud_bright(e){
  this.setData({ 
    cloud_bright: e.detail.value,
  })
},
changecloud_v(e){
  this.setData({ 
    cloud_v:e.detail.value,
  })
},

  // 通过字符串改亮度
  changeBrightByStr(e) {
    let brightStr;
    let that = this;
    // 获得亮度
    let bright = e.detail.value;
    // 获取色温
    let colorTemp = that.data.colorTemp;
    // 拼接
    brightStr = 'CCT:0,' + colorTemp + ',' + bright + '\r\n';
    console.log(brightStr);
    // 将 bright 显示出来
  that.setData({ bright: e.detail.value })
  },

  // 通过字符串改色温
  changecolorTempStr(e){
    let that = this;
    // 获得亮度
    let bright = that.data.bright;
    // 获取色温
    let colorTemp = e.detail.value;
    // 拼接
    let colorTempStr = 'CCT:0,' + colorTemp + ',' + bright + '\r\n';
    // console.log(colorTempStr);
    that.setData({ colorTemp: e.detail.value});
    // console.log(e);
    console.log(e.detail.value);
  },

 // 通过十六进制改亮度
  changebright(e) {
    let that = this;
    // bright 为亮度 0 - 100
    // bright 为number 型

    // console.log(that);

    // 如果
    let bright = e.detail.value;
    console.log(typeof bright);
    // 将 bright 显示出来
    that.setData({ bright: e.detail.value })
    // 转十六进制并补 0 之后的亮度
    let brightHex = that.zeroFillNoH(bright)
    // 嵌进编码里面， 亮度是在第 10 位
    // FA[0] 00[1] 00[2] 04[3] 00[4] 00[5] 00[6] 00[7] 15[8] 18[9] 64[10] 00[11] 00[12] 00[13] 00[14] 00[15] —— demo：表示 CCT 模式下亮度 100；色温 5400
    // 亮度在 第 10 位；色温在第 8-9 位
    // 拼接的编码
    console.log('brightHex: ', brightHex);
    let setHex = 'FA000004000000001518'.concat(brightHex , '0000000000')
    console.log(setHex);
  },

  // 发送十六进制改色温
  changecolorTemp(e){
    let that = this
    // 获得色温的十进制数
    let colorTemp = e.detail.value
    // 转十六进制并补 0 之后的色温
    let colorTempHex = that.zeroFill(colorTemp)
    console.log(colorTemp);
    console.log(colorTempHex);
    // console.log(this.data.bright);
    // console.log(typeof this.data.bright);

    // 拿到同一个功能的 亮度值 bright, 转十六进制并补 0 之后的亮度  brightHex
    let brightHex = that.zeroFillNoH(this.data.bright)
    console.log(brightHex);
    // 拼接编码，色温在第 8 - 9 位，亮度在第 10 位
    // FA[0] 00[1] 00[2] 04[3] 00[4] 00[5] 00[6] 00[7] 15[8] 18[9] 64[10] 00[11] 00[12] 00[13] 00[14] 00[15] —— demo：表示 CCT 模式下亮度 100；色温 5400
    // 亮度在 第 10 位；色温在第 8-9 位   
    let setHex = 'FA00000400000000'.concat(colorTempHex, brightHex , '0000000000')
    console.log(setHex);
    this.setData({ colorTemp: e.detail.value})
  },

  // 补零操作
  zeroFill:function(num) {
    let numL, numH;
    if(num < 16) { // 高位补两个0，低位补一个 0
      num = num.toString(16)
      // console.log('原16位', num);
      numL = '0' + num.slice(-1)
      numH = '00'.concat(num.slice(0, -2))
      num = numH.concat(numL)
    } else if (num >= 16 && num < 256) { // 高位补两个 0，低位不用补 0
      num = num.toString(16);
      // console.log('原始十六位', num);
      numL = num.slice(-2)
      numH = '00'.concat(num.slice(0, -2))
      num = numH.concat(numL)
    }else if (num >= 256 && num < 4096) { // 高位补一个 0
      num = num.toString(16);
      // console.log('原始十六位', num);
      numL = num.slice(-2)
      numH = '0'.concat(num.slice(0, -2))
      num = numH.concat(numL)
    } else {
      num = num.toString(16);
      // console.log('原始十六位', num);
      numL = num.slice(-2)
      numH = num.slice(0, -2)
    }
    // console.log(numH);
    // console.log(numL);
    // console.log(num);
    // console.log(typeof num);
    return num;
  },
  // 补零，不需要补高位
  zeroFillNoH:function(num) {
    let numL, numH;
    if(num < 16) { // 补一个 0
      num = num.toString(16)
      // console.log('原16位', num);
      numL = '0' + num.slice(-1)
      // numH = '00'.concat(num.slice(0, -2))
      num = numL
    } else { // 不用补零
      num = num.toString(16);
      // console.log('原始十六位', num);
      // numL = num.slice(-2)
      // numH = num.slice(0, -2)
    }
    // console.log(numH);
    // console.log(numL);
    // console.log(num);
    // console.log(typeof num);
    return num;
  },


  changer(e){
    this.setData({ r: e.detail.value})
  },
  changeg(e){
    this.setData({ g: e.detail.value})
  },
  changeb(e){
    this.setData({ b: e.detail.value})
  },
  changef(e){
    this.setData({ f: e.detail.value})
  },
  colorChanging(e) { 
    let color = e.currentTarget.dataset.color //获取当前slider组件的data-color值
    //let value = e.detail.value; //获取当前slider组件的value值
    //console.log(color,value) //在console中显示信息
      this.setData({
        [color]: e.detail.value //将value值赋值给数组color
      })
  },


  swiperChange: function(e) {//切换
    if(e.detail.source == 'touch') {
      let curTab = e.detail.current;
      this.setData({
        curTab: e.detail.current
      })
    }
  },

  // 顶部部光效轮播图部分
  changeSwiper: function (e) {
    this.setData({
      curTab: e.detail.current
    })
  },

  // 底部光效轮播图部分
  changeSwiper1: function (e) {
    this.setData({
      currentIndex1: e.detail.current
    })
  },

  //发送光效
  SendTap: function() {
    var that = this
    if (that.data.connected) {
      var buffer = new ArrayBuffer(that.data.inputText.length)
      var dataView = new Uint8Array(buffer)
      console.log('发送的数据：' + that.data.inputText)
      for (var i = 0; i < that.data.inputText.length; i++) {
        dataView[i] = that.data.inputText.charCodeAt(i)
      }
      wx.writeBLECharacteristicValue({
        deviceId: that.data.connectedDeviceId,
        serviceId: that.data.serviceId,
        characteristicId: that.data.characteristics[0].uuid,
        value: buffer,
        success: function(res) {
          console.log('发送成功')
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '蓝牙已断开',
        showCancel: false,
        success: function(res) {
          that.setData({
            searching: false
          })
        }
      })
    }
  },

  // 底部弹窗
  // 点击选项
  getOption:function(e){
    var that = this;
    that.setData({
      channel:e.currentTarget.dataset.value,
      hideFlag: true
    })
    let channel = that.data.channel.slice(-1) * 1 - 1
    console.log(that.data.channel);
    console.log(channel);
  },
  //取消
  mCancel: function () {
    var that = this;
    that.hideModal();
  },
  
  setChannel: function () {
    var that = this;
    that.setData({
      hideFlag: false
    })

    // 创建动画实例
    var animation = wx.createAnimation({
      duration: 400,//动画的持续时间
      timingFunction: 'ease',//动画的效果 默认值是linear->匀速，ease->动画以低速开始，然后加快，在结束前变慢
    })
    this.animation = animation; //将animation变量赋值给当前动画
    var time1 = setTimeout(function () {
      that.slideIn();//调用动画--滑入
      clearTimeout(time1);
      time1 = null;
    }, 100)
  },

  // 隐藏遮罩层
  hideModal: function () {
    var that = this;
    var animation = wx.createAnimation({
      duration: 400,//动画的持续时间 默认400ms
      timingFunction: 'ease',//动画的效果 默认值是linear
    })
    this.animation = animation
    that.slideDown();//调用动画--滑出
    var time1 = setTimeout(function () {
      that.setData({
        hideFlag: true
      })
      clearTimeout(time1);
      time1 = null;
    }, 300)//先执行下滑动画，再隐藏模块
    
  },
  //动画 -- 滑入
  slideIn: function () {
    this.animation.translateY(0).step() // 在y轴偏移，然后用step()完成一个动画
    this.setData({
      //动画实例的export方法导出动画数据传递给组件的animation属性
      animationData: this.animation.export()
    })
  },
  //动画 -- 滑出
  slideDown: function () {
    this.animation.translateY(300).step()
    this.setData({
      animationData: this.animation.export(),
    })
  },

  onLoad: function (e) {
    var that = this

   /**
   * 生命周期函数--监听页面加载
   */
    wx.getSystemInfo({
      success: res => {
        //转为rpx
        let ch = (750 / res.screenWidth) * res.windowHeight - 80;
        that.setData({
          ch
        })
      },
    })
    console.log("onloading......");
    console.log("id is ", e.id)
    that.setData({
      id:e.id
    })
    // that.getDataPoints(e.id)
    that.data.myintervalid = setInterval(function () {
      that.onShow()
    }, 3000)


    //get storage data
    var name = 'equipment_name_' + e.id
    try {
      var value = wx.getStorageSync(name)   //本地缓存中异步获取指定 name 的内容
      if (value) {
        // Do something with return value
        that.setData({
          equipment_name: value
        })
      }
    } catch (e) {
      // Do something when catch error
      console.log("get stroage data error!")
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  onShow: function (e) {
    var that = this
    // var dat = onenet.getDeviceStatus(e.id);
    // if (dat)
    //   console.log("connect to cloud success!");
    // else
    //   console.log("connect to cloud error!");

    // that.getDataPoints(that.data.id)
  },
  onHide: function () {
    // 页面隐藏
    clearInterval(this.data.myintervalid);
  },

  onUnload: function () {
    // 页面关闭
    clearInterval(this.data.myintervalid);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

    /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // btn_connect_fun: function (e) {
  //   console.log("ready to connect to onenet!");
  //   var dat = onenet.getDeviceStatus("532808382")
  //   console.log(dat)
  //   if (dat)
  //     console.log("connect to cloud success!");
  //   else
  //     console.log("connect to cloud error!");

  // },
  // btn_red_led_fun: function (e) {
  //   var that = this;
  //   if (false == e.detail.value)
  //   {
  //     console.log("ready to open red led!");
  //     onenet.sendCmd(that.data.id, "0")
  //     that.setData({
  //       redSwitchChecked: false,
  //       greenSwitchChecked: false,
  //       blueSwitchChecked: false
  //     })
  //   }else
  //   {
  //     console.log("ready to close red led!");
  //     onenet.sendCmd(that.data.id, "2")
  //     that.setData({
  //       redSwitchChecked: true,
  //       greenSwitchChecked: false,
  //       blueSwitchChecked: false,
  //       switchFlag:true
  //     })
  //   }
  // },
  // btn_green_led_fun: function (e) {
  //   var that = this;
  //   if (false == e.detail.value) {
  //     console.log("ready to open blue led!");
  //     onenet.sendCmd(that.data.id, "0")
  //     that.setData({
  //       redSwitchChecked: false,
  //       greenSwitchChecked: false,
  //       blueSwitchChecked: false
  //     })
  //   } else {
  //     console.log("ready to close blue led!");
  //     onenet.sendCmd(that.data.id, "3")
  //     that.setData({
  //       redSwitchChecked: false,
  //       greenSwitchChecked: true,
  //       blueSwitchChecked: false,
  //       switchFlag: true
  //     })
  //   }
  // },
  // btn_blue_led_fun: function (e) {
  //   var that = this;
  //   if (false == e.detail.value) {
  //     console.log("ready to open blue led!");
  //     onenet.sendCmd(that.data.id, "0")
  //     that.setData({
  //       redSwitchChecked: false,
  //       greenSwitchChecked: false,
  //       blueSwitchChecked: false
  //     })
  //   } else {
  //     console.log("ready to close blue led!");
  //     onenet.sendCmd(that.data.id, "4")
  //     that.setData({
  //       redSwitchChecked: false,
  //       greenSwitchChecked: false,
  //       blueSwitchChecked: true,
  //       switchFlag: true
  //     })
  //   }
  // },
//显示遮罩

//设置项目-
  setEquipment: function () {
    this.setData({
      showModal: true   //打开弹窗
    })
  },
  inputChange:function(e) {
    this.setData({
      input_name: e.detail.value
    })
  },
  /**
   * 弹出框蒙层截断touchmove事件
   */
  preventTouchMove: function () {
  },
  /**
   * 隐藏模态对话框
   */
  // hideModal: function () {
  //   this.setData({
  //     showModal: false    //关闭弹窗
  //   });
  // },
  /**
   * 对话框取消按钮点击事件
   */
  onCancel: function () {
    this.hideModal();
  },
  /**
   * 弹窗对话框确认按钮点击事件
   */
  onConfirm: function () {
    var that = this
    that.hideModal();
    that.setData({
      equipment_name: that.data.input_name
    })
    var name = 'equipment_name_' + that.data.id
    try {
      wx.setStorageSync(name, that.data.input_name)
    } catch (e) {
      // Do something when catch error
      console.log("setStorageSync error!")
    }
     console.log("success")
  },

  // LED控制
  switchChange: function (e) {
    console.log(e.detail.value)
  },

  //onenet interfce
  // getDataPoints: function (id) {
  //   var that = this
  //   var deviceConnected
  //   var color_value = 0
  //   //查看设备连接状态，并刷新按钮状态
  //   wx.request({
  //     url: "http://api.heclouds.com/devices/" + id + "/datapoints?datastream_id=color",
  //     header: {
  //       'content-type': 'application/x-www-form-urlencoded',
  //       "api-key": API_KEY
  //     },
  //     data: {

  //     },
  //     success(res) {
  //       console.log(res)
  //       deviceConnected = true

  //       if (that.data.switchFlag != true)
  //       {
  //         color_value = res.data.data.datastreams[0].datapoints[0].value
  //         switch (parseInt(color_value)) {
  //           case 1:
  //             that.setData({
  //               redSwitchChecked: false,
  //               greenSwitchChecked: false,
  //               blueSwitchChecked: false
  //             })
  //             break;
  //           case 2:
  //             that.setData({
  //               redSwitchChecked: true,
  //               greenSwitchChecked: false,
  //               blueSwitchChecked: false
  //             })
  //             break;
  //           case 3:
  //             that.setData({
  //               redSwitchChecked: false,
  //               greenSwitchChecked: true,
  //               blueSwitchChecked: false
  //             })
  //             break;
  //           case 4:
  //             that.setData({
  //               redSwitchChecked: false,
  //               greenSwitchChecked: false,
  //               blueSwitchChecked: true
  //             })
  //             console.log("color_value is ", color_value)
  //             console.log("blueSwitchChecked is ", that.data.blueSwitchChecked)
  //             break;
  //         }
  //         console.log("color_value is ", color_value)
  //       }else{
  //         that.setData({
  //           switchFlag:false
  //         })
  //       }
  //     },
  //     fail(res) {
  //       console.log("请求失败")
  //       deviceConnected = false
  //     }
  //   })
  // },

})