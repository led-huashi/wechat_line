var WxAutoImage = require('../../js/wxAutoImageCal.js');

var app = getApp();

Page({
  data: {
    equimentList:[
      {id:1,title:"棒灯",img:"../../images/led/led.jpg"},
      {id:2,title:"大灯",img:"../../image/led/led.jpg"},
    ],

    
    // 底部弹窗设置值
    optionList:[
      '重命名',
      '散热模式',
      '固件升级',
      '删除'
    ],
    hideFlag: true,//true-隐藏  false-显示
    animationData: {},//

    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 3000,
    duration: 1200,
    equipment_names: ["棒灯","大灯"],
    devices: [],//设备
    employId: '',
    bindDisabled:false
  },

  // 底部弹窗
  // 点击选项
  getOption:function(e){
    var that = this;
    that.setData({
      value:e.currentTarget.dataset.value,
      hideFlag: true
    })
  },
  //取消
  mCancel: function () {
    var that = this;
    that.hideModal();
  },
 
  // ----------------------------------------------------------------------modal
  // 显示遮罩层
  showModal: function () {
    var that = this;
    that.setData({
      hideFlag: false
  })

    // 创建动画实例
    var animation = wx.createAnimation({
      duration: 400,//动画的持续时间
      timingFunction: 'ease',//动画的效果 默认值是linear->匀速，ease->动画以低速开始，然后加快，在结束前变慢
    })
    this.animation = animation; //将animation变量赋值给当前动画
    var time1 = setTimeout(function () {
      that.slideIn();//调用动画--滑入
      clearTimeout(time1);
      time1 = null;
    }, 100)
  },
 
  // 隐藏遮罩层
  hideModal: function () {
    var that = this;
    var animation = wx.createAnimation({
      duration: 400,//动画的持续时间 默认400ms
      timingFunction: 'ease',//动画的效果 默认值是linear
    })
    this.animation = animation
    that.slideDown();//调用动画--滑出
    var time1 = setTimeout(function () {
      that.setData({
        hideFlag: true
      })
      clearTimeout(time1);
      time1 = null;
    }, 220)//先执行下滑动画，再隐藏模块
    
  },
  //动画 -- 滑入
  slideIn: function () {
    this.animation.translateY(0).step() // 在y轴偏移，然后用step()完成一个动画
    this.setData({
      //动画实例的export方法导出动画数据传递给组件的animation属性
      animationData: this.animation.export()
    })
  },
  //动画 -- 滑出
  slideDown: function () {
    this.animation.translateY(300).step()
    this.setData({
      animationData: this.animation.export(),
    })
  },

  onLoad: function (e){
    var that = this;
    var name = ''
    var devices_origin = ''

    console.log("onloading......");
    // setTimeout(function () {
    //   onenet.getAllDeviceStatus()
    // }, 3000);

    // 从app页面的devicesList中获取设备列表，在app.js里面
    devices_origin = app.globalData.equimentList
    for (var i = 0; i < devices_origin.length; i++) {
      name = 'equipment_name_' + devices_origin[i].id
      try {
        var value = wx.getStorageSync(name)
        if (value) {
          devices_origin[i].equipment_name = value;
        }
      } catch (e) {
        // Do something when catch error
        console.log("get stroage data error!")
      }
    }

    that.setData({
      equimentList : devices_origin
    })
    // console.log("$$$$$$$$$$$$$", that.data.devices)
  },

  onShow: function (e) {
    var that = this
    var name = ''
    var devices_origin = ''

    // 从app页面的devicesList中获取设备列表
    devices_origin = app.globalData.equimentList

    for (var i = 0; i < devices_origin.length; i++) {
      name = 'equipment_name_' + devices_origin[i].id
      try {
        var value = wx.getStorageSync(name)
        if (value) {
          devices_origin[i].equipment_name = value;
        }
      } catch (e) {
        // Do something when catch error
        console.log("get stroage data error!")
      }
    }

    that.setData({
      equimentList: devices_origin
    })

  },
  
})