
var app = getApp();
Page({
  data: {
    remind: '加载中',
    angle: 0,
    curTab: 0,
    ch: 0,
    defaultType: true,
    passwordType: true,
  },
  changeTab:function(e){
    let curTab = e.currentTarget.dataset.curtab;
    this.setData({
      curTab
    })
  },

  swiperChange:function(e){
    if(e.detail.source == 'touch') {
      let curTab = e.detail.current;
      this.setData({
        curTab
      })
    }
  },

  //defaultType：眼睛状态   passwordType：密码可见与否状态
  eyeStatus: function(){
    this.data.defaultType= !this.data.defaultType
    this.data.passwordType= !this.data.passwordType
    this.setData({
      defaultType: this.data.defaultType,
      passwordType: this.data.passwordType
  })
  },

  goToIndex: function () {
    wx.switchTab({
      // wx.navigateTo({
      // url: '/pages/Scenes/Scenes',
      url: '/pages/interface_1/interface_1',
    });
  },
  goToLogin: function () {
    wx.switchTab({
      // wx.navigateTo({
      // url: '/pages/Scenes/Scenes',
      url: '/pages/interface_1/interface_1',
    });
  },
  onLoad: function () {
    var that = this
    wx.getSystemInfo({
      success: res => {
        //转为rpx
        let ch = (750 / res.screenWidth) * res.windowHeight - 900;
        that.setData({
          ch
        })
      },
    })
    console.log("onloading......");


    // that.getDataPoints(e.id)
    that.data.myintervalid = setInterval(function () {
      that.onShow()
    }, 3000)
  },

gotoaction:function(){
  wx.navigateTo({
    url: '/pages/actiontext/actiontext',
  });
},

  onShow: function () {

  },
  onReady: function () {
    var that = this;
    setTimeout(function () {
      that.setData({
        remind: ''
      });
    }, 1000);
    wx.onAccelerometerChange(function (res) {
      var angle = -(res.x * 30).toFixed(1);
      if (angle > 14) { angle = 14; }
      else if (angle < -14) { angle = -14; }
      if (that.data.angle !== angle) {
        that.setData({
          angle: angle
        });
      }
    });
  },
  bindGetUserInfo(res) {
    var that = this
    console.log(res);
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // // 可以将 res 发送给后台解码出 unionId
              // this.globalData.userInfo = res.userInfo
              console.log("授权成功");
              this.goToIndex();  
            }
          })
        }else{
          this.goToIndex();
        }
      }
    })
    if (res.detail.userInfo) {
      console.log("点击了同意授权");
      this.goToIndex();                                                                                                        
    } else {
      console.log("点击了拒绝授权");
    }
  }
})