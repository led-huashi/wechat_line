// var WxAutoImage = require('../../js/wxAutoImageCal.js');
// var onenet = require('../../js/onenet.js');
// var API_KEY = "eCQJZEKoyVqA5qV4ef3qTH2OZzo="
var app = getApp();

Page({
  data: {
    showModal: false,
    imgList: [
      {
        id: 0,
        title: '白光',
        img: '../../image/CCT_black.png',
        imgBlue: '../../image/CCT_blue.png',
      },
      // {
      //   "title": "全彩",
      //   "img": '../../image/RGBImg.png'
      // },
      {
        id: 1,
        title: 'HSI',
        img: '../../image/HSI_black.png',
        imgBlue: '../../image/HSI_blue.png',
      },
      {
        id: 2,
        title: '爆炸',
        img: '../../image/explosion_black.png',
        imgBlue: '../../image/explosion_blue.png',
      },
      {
        id: 3,
        title: '迪厅',
        img: '../../image/disco_black.png',
        imgBlue: '../../image/disco_blue.png',
      },
      {
        id: 4,
        title: '电焊',
        img: '../../image/welding_black.png',
        imgBlue: '../../image/welding_blue.png',
      },
      {
        id: 5,
        title: 'TV',
        img: '../../image/TV_black.png',
        imgBlue: '../../image/TV_BLUE.png',
      },
      {
        id: 6,
        title: '短路',
        img: '../../image/shorts_black.png',
        imgBlue: '../../image/shorts_blue.png',
      },
      {
        id: 7,
        title: '狗仔',
        img: '../../image/paparazzi_black.png',
        imgBlue: '../../image/paparazzi_blue.png',
      },
      {
        id: 8,
        title: '呼吸',
        img: '../../image/breathe_black.png',
        imgBlue: '../../image/breathe_blue.png',
      },
      {
        id: 9,
        title: '火焰',
        img: '../../image/flame_black.png',
        imgBlue: '../../image/flame_blue.png',
      },
      {
        id: 10,
        title: '警灯',
        img: '../../image/policeLight_black.png',
        imgBlue: '../../image/policeLight_blue.png',
      },
      {
        id: 11,
        title: '蜡烛',
        img: '../../image/candles_black.png',
        imgBlue: '../../image/candles_blue.png',
      },
      {
        id: 12,
        title: '流光',
        img: '../../image/color_black.png',
        imgBlue: '../../image/color_blue.png',
      },
      {
        id: 13,
        title: '派对',
        img: '../../image/party_black.png',
        imgBlue: '../../image/party_blue.png',
      },
      {
        id: 14,
        title: '频闪',
        img: '../../image/strobe_black.png',
        imgBlue: '../../image/strobe_blue.png',
      },
      {
        id: 15,
        title: '闪电',
        img: '../../image/lightning_black.png',
        imgBlue: '../../image/lightning_blue.png',
      },
      {
        id: 16,
        title: '循环',
        img: '../../image/circulation_black.png',
        imgBlue: '../../image/circulation_blue.png',
      },
      {
        id: 17,
        title: '烟花',
        img: '../../image/fireworks_black.png',
        imgBlue: '../../image/fireworks_blue.png',
      },
      {
        id: 18,
        title: '移动',
        img: '../../image/movement_black.png',
        imgBlue: '../../image/movement_blue.png',
      },
      {
        id: 19,
        title: '音乐',
        img: '../../image/music_black.png',
        imgBlue: '../../image/music_blue.png',
      },
      {
        id: 20,
        title: '云飘过',
        img: '../../image/cloudsFloating_black.png',
        imgBlue: '../../image/cloudsFloating_blue.png',
      },
    ],

    // imgList1: [
    //   {
    //     "title":"闪电",
    //     "img": "../../image/lighting.png"
    //   },
    //     {
    //       "title":"爆炸",
    //       "img": "../../image/boom.png"
    //     },
    //     {
    //       "title":"警车",
    //       "img": "../../image/Fire_truck.png"
    //     },
    //     {
    //       "title":"警车1",
    //       "img": "../../image/Fire_truck.png"
    //     },
    //     {
    //       "title":"电视",
    //       "img": "../../image/TV.png"
    //     },
    //     {
    //       "title":"灯光",
    //       "img": "../../image/paparazzo.png"
    //     },
    //     {
    //       "title":"火花1",
    //       "img": "../../image/fire.png"
    //     },
    //     {
    //       "title":"焊接",
    //       "img": "../../image/welding.png"
    //     },
    // ],

    // 通道
    optionList: [
      {
        id: 0,
        name: '通道1',
      },
      {
        id: 1,
        name: '通道2',
      },
      {
        id: 2,
        name: '通道3',
      },
      {
        id: 3,
        name: '通道4',
      },
      {
        id: 4,
        name: '通道5',
      },
      {
        id: 5,
        name: '通道6',
      },
      {
        id: 6,
        name: '通道7',
      },
      {
        id: 7,
        name: '通道8',
      },
    ],

    // 车型
    car: [
      {
        id: 0,
        title: '警车',
        img: '../../image/police_gray.png',
        imgBlue: '../../image/police_blue.png',
      },
      {
        id: 1,
        title: '救护车',
        img: '../../image/ambulance_gray.png',
        imgBlue: '../../image/ambulance_blue.png',
      },
      {
        id: 2,
        title: '消防车',
        img: '../../image/fireEngine_gray.png',
        imgBlue: '../../image/fireEngine_blue.png',
      },
    ],
    // 类型
    type: [
      {
        id: 0,
      },
      {
        id: 1,
      },
      {
        id: 2,
      },
    ],
    type1: [
      {
        id: 0,
      },
      {
        id: 1,
      },
    ],
    ch: 0,
    curTab: 0,
    equipment_name: '',
    input_name: '',
    id: 0,
    redSwitchChecked: false,
    greenSwitchChecked: false,
    blueSwitchChecked: false,
    switchFlag: false,
    myintervalid: 0,

    // 通道
    channel: '通道1',
    bright: 50, //亮度
    // brightHex: "32",    // 亮度的十六进制
    colorTemp: 5400, //色温
    r: 128,
    g: 128,
    b: 128,
    H: 180, // H
    S: 50, // S
    I: 100, // I
    trigger: 0, // 0 为爆炸触发 1 为不触发
    triggerScan: false, // 触发转换初始值
    boomBright: 100, // 爆炸的亮度
    boomF: 1, // 爆炸的频率
    discoColor: -1, // 迪厅颜色变化
    discoBright: 100, // 迪厅亮度
    discoF: 1, // 迪厅频率
    electricWeldingType: -1, // 电焊类型
    electricWeldingBright: 100, // 电焊亮度
    electricWeldingF: 1, // 电焊频率
    TVColorTemp: -1, // 电视色温
    TVBright: 100, // 电视亮度
    TVF: 1, // 电视频率
    shortCircuitSpeed: -1, // 短路速度
    shortCircuitBright: 100, // 短路亮度
    shortCircuitF: 1, // 短路频率
    paparazziType: -1, // 狗仔类型
    paparazziBright: 100, // 狗仔亮度
    paparazziF: 1, // 狗仔频率
    breatheSpeed: -1, // 呼吸速度
    breatheBright: 100, // 呼吸亮度
    breatheF: 1, // 呼吸频率
    fireColorTemp: -1, // 火焰色温
    fireBright: 100, // 火焰温度
    fireF: 1, // 火焰频率
    changeCar: false, // false为未选中警车类型, true为选中警车类型
    policeLightsType: -1, // 警灯类型
    policeLightsBright: 100, // 警灯亮度
    policeLightsF: 1, // 警灯 频率
    candlesColorTemp: -1, // 蜡烛色温
    candlesBright: 100, // 蜡烛亮度
    candlesF: 1, // 蜡烛频率
    colorType: -1, // 流光类型
    colorBright: 100, // 流光亮度
    colorF: 1, // 流光速度
    partyType: -1, // 派对类型
    partyBright: 100, // 派对亮度
    partyF: 1, // 派对频率
    strobeBright: 100, // 频闪亮度
    strobeF: 0, // 频闪频率
    lightningSpeed: -1, // 闪电速度
    lightningBright: 100, // 闪电亮度
    lightningF: 1, // 闪电频率
    circulationType: -1, // 循环类型
    circulationBright: 100, // 循环亮度
    circulationF: 1, // 循环频率
    fireworksColor: -1, // 烟花 颜色
    fireworksBright: 100, // 烟花亮度
    fireworksF: 1, // 烟花频率
    moveType: -1, // 移动类型
    moveBright: 100, // 移动亮度
    moveF: 1, // 移动速度
    musicType: -1, // 音乐模式
    musicBright: 100, // 音乐亮度
    cloudType: -1, // 云飘过类型
    cloudBright: 100, // 云飘过亮度
    cloudF: 1, // 云飘过频率

    // 通信端
    inputText: 'a',
    echoText: '',
    receiveText: '',
    receiveArry: [],
    name: '',
    connectedDeviceId: '',
    serviceId: 0,
    characteristics: {},
    connected: true,

    // 弹窗
    hideFlag: true, //true-隐藏  false-显示
    animationData: {}, //
  },

  // 通过字符串改亮度
  changeBrightByStr(e) {
    let brightStr;
    let that = this;
    // 获得亮度
    let bright = e.detail.value;
    // 获取色温
    let colorTemp = that.data.colorTemp;
    // 将 bright 显示出来
    that.setData({
      bright: e.detail.value,
    });
    let channel = that.data.channel.slice(-1) * 1 - 1;
    // 拼接
    brightStr = 'CCT:' + channel + ',' + colorTemp + ',' + bright + '\r\n';
    console.log(brightStr);
    // 发送亮度信息
    that.SendTap(brightStr);
  },

  // 通过字符串改色温
  changecolorTempStr(e) {
    let that = this;
    // 获得亮度
    let bright = that.data.bright;
    // 获取色温
    let colorTemp = e.detail.value;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    // 拼接
    let colorTempStr =
      'CCT:' + channel + ',' + colorTemp + ',' + bright + '\r\n';
    console.log(colorTempStr);
    that.setData({ colorTemp: e.detail.value });
    that.SendTap(colorTempStr);
  },

  // 改 HSI 的 H
  changeH(e) {
    let that = this;
    // 获得 H
    let H = e.detail.value;
    // 获得 S
    let S = that.data.S;
    // 获得 I
    let I = that.data.I;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let HSIStr = 'HSI:' + channel + ',' + H + ',' + S + ',' + I + '\r\n';
    console.log(HSIStr);
    that.setData({ H: e.detail.value });
    that.SendTap(HSIStr);
  },
  // 改 HSI 的 S
  changeS(e) {
    let that = this;
    // 获得 H
    let H = that.data.H;
    // 获得 S
    let S = e.detail.value;
    // 获得 I
    let I = that.data.I;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let HSIStr = 'HSI:' + channel + ',' + H + ',' + S + ',' + I + '\r\n';
    console.log(HSIStr);
    that.setData({ S: e.detail.value });
    that.SendTap(HSIStr);
  },
  // 改 HSI 的 I
  changeI(e) {
    let that = this;
    // 获得 H
    let H = that.data.H;
    // 获得 S
    let S = that.data.S;
    // 获得 I
    let I = e.detail.value;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let HSIStr = 'HSI:' + channel + ',' + H + ',' + S + ',' + I + '\r\n';
    console.log(HSIStr);
    that.setData({ I: e.detail.value });
    that.SendTap(HSIStr);
  },

  // 改爆炸 触发
  changeBoomTrigger(e) {
    let that = this;
    // 获得 触发
    if (that.data.triggerScan == true) {
      that.data.triggerScan = false;
      that.setData({
        trigger: 1,
        triggerScan: false,
      });
    } else {
      that.data.triggerScan = true;
      that.setData({
        trigger: 0,
        triggerScan: true,
      });
    }
    console.log('triggerScan：', that.data.triggerScan);
    let trigger = that.data.trigger;
    // 获得 亮度
    let boomBright = that.data.boomBright;
    // 获得 频率
    let boomF = that.data.boomF;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let boomStr =
      'FX:' +
      channel +
      ',16,' +
      trigger +
      ',' +
      boomBright +
      ',' +
      boomF +
      '\r\n';
    console.log(boomStr);
    // that.setData({ trigger: e.detail.value });
    that.SendTap(boomStr);
  },
  // 改爆炸 亮度
  changeBoomBright(e) {
    let that = this;
    // 获得 触发
    let trigger = that.data.trigger;
    // 获得 亮度
    let boomBright = e.detail.value;
    // 获得 频率
    let boomF = that.data.boomF;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let boomStr =
      'FX:' +
      channel +
      ',16,' +
      trigger +
      ',' +
      boomBright +
      ',' +
      boomF +
      '\r\n';
    console.log(boomStr);
    that.setData({ boomBright: e.detail.value });
    that.SendTap(boomStr);
  },
  // 改爆炸 频率
  changeBoomF(e) {
    let that = this;
    // 获得 触发
    let trigger = that.data.trigger;
    // 获得 亮度
    let boomBright = that.data.boomBright;
    // 获得 频率
    let boomF = e.detail.value;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let boomStr =
      'FX:' +
      channel +
      ',16,' +
      trigger +
      ',' +
      boomBright +
      ',' +
      boomF +
      '\r\n';
    console.log(boomStr);
    that.setData({ boomF: e.detail.value });
    that.SendTap(boomStr);
  },

  // 改迪厅 颜色种类
  changeDiscoColor(e) {
    let that = this;
    // 获得 触发
    that.setData({ discoColor: e.currentTarget.id });
    let discoColor = that.data.discoColor;
    // 获得 亮度
    let discoBright = that.data.discoBright;
    // 获得 频率
    let discoF = that.data.discoF;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let discoStr =
      'FX:' +
      channel +
      ',14,' +
      discoColor +
      ',' +
      discoBright +
      ',' +
      discoF +
      '\r\n';
    console.log(discoStr);

    that.SendTap(discoStr);
  },
  // 改迪厅 亮度
  changeDiscoBright(e) {
    let that = this;
    // 获得 触发
    let discoColor = that.data.discoColor;
    // 获得 亮度
    let discoBright = e.detail.value;
    // 获得 频率
    let discoF = that.data.discoF;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let discoStr =
      'FX:' +
      channel +
      ',14,' +
      discoColor +
      ',' +
      discoBright +
      ',' +
      discoF +
      '\r\n';
    console.log(discoStr);
    that.setData({ discoBright: e.detail.value });
    that.SendTap(discoStr);
  },
  // 改迪厅 频率
  changeDiscoF(e) {
    let that = this;
    // 获得 触发
    let discoColor = that.data.discoColor;
    // 获得 亮度
    let discoBright = that.data.discoBright;
    // 获得 频率
    let discoF = e.detail.value;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let discoStr =
      'FX:' +
      channel +
      ',14,' +
      discoColor +
      ',' +
      discoBright +
      ',' +
      discoF +
      '\r\n';
    console.log(discoStr);
    that.setData({ discoF: e.detail.value });
    that.SendTap(discoStr);
  },

  // 改电焊 类型
  changeElectricWeldingType(e) {
    let that = this;
    // 获得 触发
    that.setData({ electricWeldingType: e.currentTarget.id });
    let electricWeldingType = that.data.electricWeldingType;
    // 获得 亮度
    let electricWeldingBright = that.data.electricWeldingBright;
    // 获得 频率
    let electricWeldingF = that.data.electricWeldingF;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let electricWeldingStr =
      'FX:' +
      channel +
      ',13,' +
      electricWeldingType +
      ',' +
      electricWeldingBright +
      ',' +
      electricWeldingF +
      '\r\n';
    console.log(electricWeldingStr);

    that.SendTap(electricWeldingStr);
  },
  // 改电焊 亮度
  changEelectricWeldingBright(e) {
    let that = this;
    // 获得 触发
    let electricWeldingType = that.data.electricWeldingType;
    // 获得 亮度
    let electricWeldingBright = e.detail.value;
    // 获得 频率
    let electricWeldingF = that.data.electricWeldingF;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let electricWeldingStr =
      'FX:' +
      channel +
      ',13,' +
      electricWeldingType +
      ',' +
      electricWeldingBright +
      ',' +
      electricWeldingF +
      '\r\n';
    console.log(electricWeldingStr);
    that.setData({ electricWeldingBright: e.detail.value });
    that.SendTap(electricWeldingStr);
  },
  // 改电焊 频率
  changeElectricWeldingF(e) {
    let that = this;
    // 获得 触发
    let electricWeldingType = that.data.electricWeldingType;
    // 获得 亮度
    let electricWeldingBright = that.data.electricWeldingBright;
    // 获得 频率
    let electricWeldingF = e.detail.value;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let electricWeldingStr =
      'FX:' +
      channel +
      ',13,' +
      electricWeldingType +
      ',' +
      electricWeldingBright +
      ',' +
      electricWeldingF +
      '\r\n';
    console.log(electricWeldingStr);
    that.setData({ electricWeldingF: e.detail.value });
    that.SendTap(electricWeldingStr);
  },

  // 改电视 色温类型
  changeTVColorTemp(e) {
    let that = this;
    // 获得 触发
    that.setData({ TVColorTemp: e.currentTarget.id });
    let TVColorTemp = that.data.TVColorTemp;
    // 获得 亮度
    let TVBright = that.data.TVBright;
    // 获得 频率
    let TVF = that.data.TVF;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let TVStr =
      'FX:' +
      channel +
      ',12,' +
      TVColorTemp +
      ',' +
      TVBright +
      ',' +
      TVF +
      '\r\n';
    console.log(TVStr);

    that.SendTap(TVStr);
  },
  // 改电视 亮度
  changeTVBright(e) {
    let that = this;
    // 获得 触发
    let TVColorTemp = that.data.TVColorTemp;
    // 获得 亮度
    let TVBright = e.detail.value;
    // 获得 频率
    let TVF = that.data.TVF;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let TVStr =
      'FX:' +
      channel +
      ',12,' +
      TVColorTemp +
      ',' +
      TVBright +
      ',' +
      TVF +
      '\r\n';
    console.log(TVStr);
    that.setData({ TVBright: e.detail.value });
    that.SendTap(TVStr);
  },
  // 改电视 频率
  changeTVF(e) {
    let that = this;
    // 获得 触发
    let TVColorTemp = that.data.TVColorTemp;
    // 获得 亮度
    let TVBright = that.data.TVBright;
    // 获得 频率
    let TVF = e.detail.value;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let TVStr =
      'FX:' +
      channel +
      ',12,' +
      TVColorTemp +
      ',' +
      TVBright +
      ',' +
      TVF +
      '\r\n';
    console.log(TVStr);
    that.setData({ TVF: e.detail.value });
    that.SendTap(TVStr);
  },

  // 改短路 速度
  changeShortCircuitSpeed(e) {
    let that = this;
    // 获得 触发
    that.setData({ shortCircuitSpeed: e.currentTarget.id });
    let shortCircuitSpeed = that.data.shortCircuitSpeed;
    // 获得 亮度
    let shortCircuitBright = that.data.shortCircuitBright;
    // 获得 频率
    let shortCircuitF = that.data.shortCircuitF;
    // 拼接
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let shortCircuitStr =
      'FX:' +
      channel +
      ',11,' +
      shortCircuitSpeed +
      ',' +
      shortCircuitBright +
      ',' +
      shortCircuitF +
      '\r\n';
    console.log(shortCircuitStr);

    that.SendTap(shortCircuitStr);
  },
  // 改短路 亮度
  changeShortCircuitBright(e) {
    let that = this;
    // 获得 触发
    let shortCircuitSpeed = that.data.shortCircuitSpeed;
    // 获得 亮度
    let shortCircuitBright = e.detail.value;
    // 获得 频率
    let shortCircuitF = that.data.shortCircuitF;
    // 拼接
    let shortCircuitStr =
      'FX:' +
      channel +
      ',11,' +
      shortCircuitSpeed +
      ',' +
      shortCircuitBright +
      ',' +
      shortCircuitF +
      '\r\n';
    console.log(shortCircuitStr);
    that.setData({ shortCircuitBright: e.detail.value });
    that.SendTap(shortCircuitStr);
  },
  // 改短路 频率
  changeShortCircuitF(e) {
    let that = this;
    // 获得 触发
    let shortCircuitSpeed = that.data.shortCircuitSpeed;
    // 获得 亮度
    let shortCircuitBright = that.data.shortCircuitBright;
    // 拼接
    // 获得 频率
    let shortCircuitF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let shortCircuitStr =
      'FX:' +
      channel +
      ',11,' +
      shortCircuitSpeed +
      ',' +
      shortCircuitBright +
      ',' +
      shortCircuitF +
      '\r\n';
    console.log(shortCircuitStr);
    that.setData({ shortCircuitF: e.detail.value });
    that.SendTap(shortCircuitStr);
  },

  // 改狗仔 类型
  changePaparazziType(e) {
    let that = this;
    // 获得 触发
    that.setData({ paparazziType: e.currentTarget.id });
    let paparazziType = that.data.paparazziType;
    // 获得 亮度
    let paparazziBright = that.data.paparazziBright;
    // 拼接
    // 获得 频率
    let paparazziF = that.data.paparazziF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let paparazziStr =
      'FX:' +
      channel +
      ',10,' +
      paparazziType +
      ',' +
      paparazziBright +
      ',' +
      paparazziF +
      '\r\n';
    console.log(paparazziStr);

    that.SendTap(paparazziStr);
  },
  // 改狗仔 亮度
  changePaparazziBright(e) {
    let that = this;
    // 获得 触发
    let paparazziType = that.data.paparazziType;
    // 获得 亮度
    let paparazziBright = e.detail.value;
    // 拼接
    // 获得 频率
    let paparazziF = that.data.paparazziF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let paparazziStr =
      'FX:' +
      channel +
      ',10,' +
      paparazziType +
      ',' +
      paparazziBright +
      ',' +
      paparazziF +
      '\r\n';
    console.log(paparazziStr);
    that.setData({ paparazziBright: e.detail.value });
    that.SendTap(paparazziStr);
  },
  // 改狗仔 频率
  changePaparazziF(e) {
    let that = this;
    // 获得 触发
    let paparazziType = that.data.paparazziType;
    // 获得 亮度
    let paparazziBright = that.data.paparazziBright;
    // 拼接
    // 获得 频率
    let paparazziF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let paparazziStr =
      'FX:' +
      channel +
      ',10,' +
      paparazziType +
      ',' +
      paparazziBright +
      ',' +
      paparazziF +
      '\r\n';
    console.log(paparazziStr);
    that.setData({ paparazziF: e.detail.value });
    that.SendTap(paparazziStr);
  },

  // 改呼吸 速度
  changeBreatheSpeed(e) {
    let that = this;
    // 获得 触发
    that.setData({ breatheSpeed: e.currentTarget.id });
    let breatheSpeed = that.data.breatheSpeed;
    // 获得 亮度
    let breatheBright = that.data.breatheBright;
    // 拼接
    // 获得 频率
    let breatheF = that.data.breatheF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let breatheStr =
      'FX:' +
      channel +
      ',9,' +
      breatheSpeed +
      ',' +
      breatheBright +
      ',' +
      breatheF +
      '\r\n';
    console.log(breatheStr);

    that.SendTap(breatheStr);
  },
  // 改呼吸 亮度
  changeBreatheBright(e) {
    let that = this;
    // 获得 触发
    let breatheSpeed = that.data.breatheSpeed;
    // 获得 亮度
    let breatheBright = e.detail.value;
    // 获得 频率
    let breatheF = that.data.breatheF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let breatheStr =
      'FX:' +
      channel +
      ',9,' +
      breatheSpeed +
      ',' +
      breatheBright +
      ',' +
      breatheF +
      '\r\n';
    console.log(breatheStr);
    that.setData({ breatheBright: e.detail.value });
    that.SendTap(breatheStr);
  },
  // 改呼吸 频率
  changeBreatheF(e) {
    let that = this;
    // 获得 触发
    let breatheSpeed = that.data.breatheSpeed;
    // 获得 亮度
    let breatheBright = that.data.breatheBright;
    // 获得 频率
    let breatheF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let breatheStr =
      'FX:' +
      channel +
      ',9,' +
      breatheSpeed +
      ',' +
      breatheBright +
      ',' +
      breatheF +
      '\r\n';
    console.log(breatheStr);
    that.setData({ breatheF: e.detail.value });
    that.SendTap(breatheStr);
  },

  // 改火焰 色温
  changeFireColorTemp(e) {
    let that = this;
    // 获得 触发
    that.setData({ fireColorTemp: e.currentTarget.id });
    let fireColorTemp = that.data.fireColorTemp;
    // 获得 亮度
    let fireBright = that.data.fireBright;
    // 获得 频率
    let fireF = that.data.fireF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let fireStr =
      'FX:' +
      channel +
      ',8,' +
      fireColorTemp +
      ',' +
      fireBright +
      ',' +
      fireF +
      '\r\n';
    console.log(fireStr);

    that.SendTap(fireStr);
  },
  // 改火焰 亮度
  changeFireBright(e) {
    let that = this;
    // 获得 触发
    let fireColorTemp = that.data.fireColorTemp;
    // 获得 亮度
    let fireBright = e.detail.value;
    // 获得 频率
    let fireF = that.data.fireF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let fireStr =
      'FX:' +
      channel +
      ',8,' +
      fireColorTemp +
      ',' +
      fireBright +
      ',' +
      fireF +
      '\r\n';
    console.log(fireStr);
    that.setData({ fireBright: e.detail.value });
    that.SendTap(fireStr);
  },
  // 改火焰 频率
  changeFireF(e) {
    let that = this;
    // 获得 触发
    let fireColorTemp = that.data.fireColorTemp;
    // 获得 亮度
    let fireBright = that.data.fireBright;
    // 获得 频率
    let fireF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let fireStr =
      'FX:' +
      channel +
      ',8,' +
      fireColorTemp +
      ',' +
      fireBright +
      ',' +
      fireF +
      '\r\n';
    console.log(fireStr);
    that.setData({ fireF: e.detail.value });
    that.SendTap(fireStr);
  },

  // 改警灯 类型
  changePoliceLightsType(e) {
    let that = this;
    // 获得 触发
    that.setData({
      policeLightsType: e.currentTarget.id,
      // changeCar: false
    });
    let policeLightsType = that.data.policeLightsType;
    // console.log(policeLightsType);
    // console.log(e.currentTarget.id);
    // 获得 亮度
    let policeLightsBright = that.data.policeLightsBright;
    // 获得 频率
    let policeLightsF = that.data.policeLightsF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let policeLightsStr =
      'FX:' +
      channel +
      ',7,' +
      policeLightsType +
      ',' +
      policeLightsBright +
      ',' +
      policeLightsF +
      '\r\n';
    console.log(policeLightsStr);
    // that.setData({
    //   policeLightsType: e.currentTarget.id,
    //   // changeCar: false
    // });
    that.SendTap(policeLightsStr);
  },
  // 改警灯 亮度
  changePoliceLightsBright(e) {
    let that = this;
    // 获得 触发
    let policeLightsType = that.data.policeLightsType;
    // 获得 亮度
    let policeLightsBright = e.detail.value;
    // 获得 频率
    let policeLightsF = that.data.policeLightsF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let policeLightsStr =
      'FX:' +
      channel +
      ',7,' +
      policeLightsType +
      ',' +
      policeLightsBright +
      ',' +
      policeLightsF +
      '\r\n';
    console.log(policeLightsStr);
    that.setData({ policeLightsBright: e.detail.value });
    that.SendTap(policeLightsStr);
  },
  // 改警灯 频率
  changePoliceLightsF(e) {
    let that = this;
    // 获得 触发
    let policeLightsType = that.data.policeLightsType;
    // 获得 亮度
    let policeLightsBright = that.data.policeLightsBright;
    // 获得 频率
    let policeLightsF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let policeLightsStr =
      'FX:' +
      channel +
      ',7,' +
      policeLightsType +
      ',' +
      policeLightsBright +
      ',' +
      policeLightsF +
      '\r\n';
    console.log(policeLightsStr);
    that.setData({ policeLightsF: e.detail.value });
    that.SendTap(policeLightsStr);
  },

  // 改蜡烛 类型
  changeCandlesColorTemp(e) {
    let that = this;
    // 获得 触发
    that.setData({
      candlesColorTemp: e.currentTarget.id,
    });
    let candlesColorTemp = that.data.candlesColorTemp;
    // let candlesColorTemp = e.detail.value;
    console.log(e.currentTarget.id);
    // 获得 亮度
    let candlesBright = that.data.candlesBright;
    // 获得 频率
    let candlesF = that.data.candlesF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let candlesStr =
      'FX:' +
      channel +
      ',0,' +
      candlesColorTemp +
      ',' +
      candlesBright +
      ',' +
      candlesF +
      '\r\n';
    console.log(candlesStr);
    that.SendTap(candlesStr);
  },
  // 改蜡烛 亮度
  changeCandlesBright(e) {
    let that = this;
    // 获得 触发
    let candlesColorTemp = that.data.candlesColorTemp;
    // 获得 亮度
    let candlesBright = e.detail.value;
    // 获得 频率
    let candlesF = that.data.candlesF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let candlesStr =
      'FX:' +
      channel +
      ',0,' +
      candlesColorTemp +
      ',' +
      candlesBright +
      ',' +
      candlesF +
      '\r\n';
    console.log(candlesStr);
    that.setData({ candlesBright: e.detail.value });
    that.SendTap(candlesStr);
  },
  // 改蜡烛 频率
  changeCandlesF(e) {
    let that = this;
    // 获得 触发
    let candlesColorTemp = that.data.candlesColorTemp;
    // 获得 亮度
    let candlesBright = that.data.candlesBright;
    // 获得 频率
    let candlesF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let candlesStr =
      'FX:' +
      channel +
      ',0,' +
      candlesColorTemp +
      ',' +
      candlesBright +
      ',' +
      candlesF +
      '\r\n';
    console.log(candlesStr);
    that.setData({ candlesF: e.detail.value });
    that.SendTap(candlesStr);
  },

  // 改流光 类型
  changeColorType(e) {
    let that = this;
    // 获得 触发
    that.setData({ colorType: e.currentTarget.id });
    let colorType = that.data.colorType;
    // 获得 亮度
    let colorBright = that.data.colorBright;
    // 获得 频率
    let colorF = that.data.colorF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let colorStr =
      'FX:' +
      channel +
      ',6,' +
      colorType +
      ',' +
      colorBright +
      ',' +
      colorF +
      '\r\n';
    console.log(colorStr);

    that.SendTap(colorStr);
  },
  // 改流光 亮度
  changeColorBright(e) {
    let that = this;
    // 获得 触发
    let colorType = that.data.colorType;
    // 获得 亮度
    let colorBright = e.detail.value;
    // 获得 频率
    let colorF = that.data.colorF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let colorStr =
      'FX:' +
      channel +
      ',6,' +
      colorType +
      ',' +
      colorBright +
      ',' +
      colorF +
      '\r\n';
    console.log(colorStr);
    that.setData({ colorBright: e.detail.value });
    that.SendTap(colorStr);
  },
  // 改流光 速度
  changeColorF(e) {
    let that = this;
    // 获得 触发
    let colorType = that.data.colorType;
    // 获得 亮度
    let colorBright = that.data.colorBright;
    // 获得 频率
    let colorF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let colorStr =
      'FX:' +
      channel +
      ',6,' +
      colorType +
      ',' +
      colorBright +
      ',' +
      colorF +
      '\r\n';
    console.log(colorStr);
    that.setData({ colorF: e.detail.value });
    that.SendTap(colorStr);
  },

  // 改派对 类型
  changePartyType(e) {
    let that = this;
    // 获得 触发
    that.setData({ partyType: e.currentTarget.id });
    let partyType = that.data.partyType;
    // 获得 亮度
    let partyBright = that.data.partyBright;
    // 获得 频率
    let partyF = that.data.partyF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let partyStr =
      'FX:' +
      channel +
      ',18,' +
      partyType +
      ',' +
      partyBright +
      ',' +
      partyF +
      '\r\n';
    console.log(partyStr);

    that.SendTap(partyStr);
  },
  // 改派对 亮度
  changePartyBright(e) {
    let that = this;
    // 获得 触发
    let partyType = that.data.partyType;
    // 获得 亮度
    let partyBright = e.detail.value;
    // 获得 频率
    let partyF = that.data.partyF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let partyStr =
      'FX:' +
      channel +
      ',18,' +
      partyType +
      ',' +
      partyBright +
      ',' +
      partyF +
      '\r\n';
    console.log(partyStr);
    that.setData({ partyBright: e.detail.value });
    that.SendTap(partyStr);
  },
  // 改派对 频率
  changePartyF(e) {
    let that = this;
    // 获得 触发
    let partyType = that.data.partyType;
    // 获得 亮度
    let partyBright = that.data.partyBright;
    // 获得 频率
    let partyF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let partyStr =
      'FX:' +
      channel +
      ',18,' +
      partyType +
      ',' +
      partyBright +
      ',' +
      partyF +
      '\r\n';
    console.log(partyStr);
    that.setData({ partyF: e.detail.value });
    that.SendTap(partyStr);
  },

  // 改频闪 亮度
  changeStrobeBright(e) {
    let that = this;
    // 获得 亮度
    let strobeBright = e.detail.value;
    // 获得 频率
    let strobeF = that.data.strobeF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let strobeStr =
      'FX:' + channel + ',5,' + strobeBright + ',' + strobeF + '\r\n';
    console.log(strobeStr);
    that.setData({ strobeBright: e.detail.value });
    that.SendTap(strobeStr);
  },
  // 改频闪 频率
  changeStrobeF(e) {
    let that = this;
    // 获得 亮度
    let strobeBright = that.data.strobeBright;
    // 获得 频率
    let strobeF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let strobeStr =
      'FX:' + channel + ',5,' + strobeBright + ',' + strobeF + '\r\n';
    console.log(strobeStr);
    that.setData({ strobeF: e.detail.value });
    that.SendTap(strobeStr);
  },

  // 闪电 速度
  changeLightningSpeed(e) {
    let that = this;
    // 获得 亮度
    that.setData({ lightningSpeed: e.currentTarget.id });
    let lightningSpeed = that.data.lightningSpeed;
    // 获得 频率
    let lightningBright = that.data.lightningBright;
    let lightningF = that.data.lightningF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let lightningStr =
      'FX:' +
      channel +
      ',17,' +
      lightningSpeed +
      ',' +
      lightningBright +
      ',' +
      lightningF +
      '\r\n';
    console.log(lightningStr);

    that.SendTap(lightningStr);
  },
  // 闪电 亮度
  changeLightningBright(e) {
    let that = this;
    // 获得 亮度
    let lightningSpeed = that.data.lightningSpeed;
    // 获得 频率
    let lightningBright = e.detail.value;
    let lightningF = that.data.lightningF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let lightningStr =
      'X:' +
      channel +
      ',17,' +
      lightningSpeed +
      ',' +
      lightningBright +
      ',' +
      lightningF +
      '\r\n';
    console.log(lightningStr);
    that.setData({ lightningBright: e.detail.value });
    that.SendTap(lightningStr);
  },
  // 闪电 频率
  changeLightningF(e) {
    let that = this;
    // 获得 亮度
    let lightningSpeed = that.data.lightningSpeed;
    // 获得 频率
    let lightningBright = that.data.lightningBright;
    let lightningF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let lightningStr =
      'FX:' +
      channel +
      ',17,' +
      lightningSpeed +
      ',' +
      lightningBright +
      ',' +
      lightningF +
      '\r\n';
    console.log(lightningStr);
    that.setData({ lightningF: e.detail.value });
    that.SendTap(lightningStr);
  },

  // 改循环 色彩范围  —— 好像是无的
  changeCirculationType(e) {
    let that = this;
    // 获得 亮度
    that.setData({ circulationType: e.currentTarget.id });
    let circulationType = that.data.circulationType;
    // 获得 频率
    let circulationBright = that.data.circulationBright;
    let circulationF = that.data.circulationF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let circulationStr =
      'FX:' +
      channel +
      ',4,' +
      circulationType +
      ',' +
      circulationBright +
      ',' +
      circulationF +
      '\r\n';
    console.log(circulationStr);

    that.SendTap(circulationStr);
  },
  // 改循环 亮度
  changeCirculationBright(e) {
    let that = this;
    // 获得 亮度
    let circulationType = that.data.circulationType;
    // 获得 频率
    let circulationBright = e.detail.value;
    let circulationF = that.data.circulationF;
    let circulationStr =
      'FX:' +
      channel +
      ',4,' +
      circulationType +
      ',' +
      circulationBright +
      ',' +
      circulationF +
      '\r\n';
    console.log(circulationStr);
    that.setData({ circulationBright: e.detail.value });
    that.SendTap(circulationStr);
  },
  // 改循环 频率
  changeCirculationF(e) {
    let that = this;
    // 获得 亮度
    let circulationType = that.data.circulationType;
    // 获得 频率
    let circulationBright = that.data.circulationF;
    let circulationF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let circulationStr =
      'FX:' +
      channel +
      ',4,' +
      circulationType +
      ',' +
      circulationBright +
      ',' +
      circulationF +
      '\r\n';
    console.log(circulationStr);
    that.setData({ circulationF: e.detail.value });
    that.SendTap(circulationStr);
  },

  // 改烟花 颜色
  changeFireworksColor(e) {
    let that = this;
    // 获得 亮度
    that.setData({ fireworksColor: e.currentTarget.id });
    let fireworksColor = that.data.fireworksColor;
    // 获得 频率
    let fireworksBright = that.data.fireworksBright;
    let fireworksF = that.data.fireworksF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let fireworksStr =
      'FX:' +
      channel +
      ',3,' +
      fireworksColor +
      ',' +
      fireworksBright +
      ',' +
      fireworksF +
      '\r\n';
    console.log(fireworksStr);

    that.SendTap(fireworksStr);
  },
  // 改烟花 亮度
  changeFireworksBright(e) {
    let that = this;
    // 获得 亮度
    let fireworksColor = that.data.fireworksColor;
    // 获得 频率
    let fireworksBright = e.detail.value;
    let fireworksF = that.data.fireworksF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let fireworksStr =
      'FX:' +
      channel +
      ',3,' +
      fireworksColor +
      ',' +
      fireworksBright +
      ',' +
      fireworksF +
      '\r\n';
    console.log(fireworksStr);
    that.setData({ fireworksBright: e.detail.value });
    that.SendTap(fireworksStr);
  },
  // 改烟花 频率
  changeFireworksF(e) {
    let that = this;
    // 获得 亮度
    let fireworksColor = that.data.fireworksColor;
    // 获得 频率
    let fireworksBright = that.data.fireworksBright;
    let fireworksF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let fireworksStr =
      'FX:' +
      channel +
      ',3,' +
      fireworksColor +
      ',' +
      fireworksBright +
      ',' +
      fireworksF +
      '\r\n';
    console.log(fireworksStr);
    that.setData({ fireworksF: e.detail.value });
    that.SendTap(fireworksStr);
  },

  // 改移动 颜色类型
  changeMoveType(e) {
    let that = this;
    // 获得 亮度\
    that.setData({ moveType: e.currentTarget.id });
    let moveType = that.data.moveType;
    // 获得 频率
    let moveBright = that.data.moveBright;
    let moveF = that.data.moveF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let moveStr =
      'FX:' +
      channel +
      ',4,' +
      moveType +
      ',' +
      moveBright +
      ',' +
      moveF +
      '\r\n';
    console.log(moveStr);

    that.SendTap(moveStr);
  },
  // 改移动 亮度
  changeMoveBright(e) {
    let that = this;
    // 获得 亮度
    let moveType = that.data.moveType;
    // 获得 频率
    let moveBright = e.detail.value;
    let moveF = that.data.moveF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let moveStr =
      'FX:' +
      channel +
      ',4,' +
      moveType +
      ',' +
      moveBright +
      ',' +
      moveF +
      '\r\n';
    console.log(moveStr);
    that.setData({ moveBright: e.detail.value });
    that.SendTap(moveStr);
  },
  // 改移动 频率
  changeMoveF(e) {
    let that = this;
    // 获得 亮度
    let moveType = that.data.moveType;
    // 获得 频率
    let moveBright = that.data.moveBright;
    let moveF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let moveStr =
      'FX:' +
      channel +
      ',4,' +
      moveType +
      ',' +
      moveBright +
      ',' +
      moveF +
      '\r\n';
    console.log(moveStr);
    that.setData({ moveF: e.detail.value });
    that.SendTap(moveStr);
  },

  // 改音乐 模式
  changeMusicType(e) {
    let that = this;
    // 获得 亮度
    that.setData({
      musicType: e.currentTarget.id,
    });
    let musicType = that.data.musicType;
    // 获得 频率
    let musicBright = that.data.musicBright;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let musicStr =
      'FX:' + channel + ',2,' + musicType + ',' + musicBright + '\r\n';
    console.log(musicStr);

    that.SendTap(musicStr);
  },
  // 改音乐 亮度
  changeMusicBright(e) {
    let that = this;
    // 获得 亮度
    let musicType = that.data.musicType;
    // 获得 频率
    let musicBright = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let musicStr =
      'FX:' + channel + ',2,' + musicType + ',' + musicBright + '\r\n';
    console.log(musicStr);
    that.setData({ musicBright: e.detail.value });
    that.SendTap(musicStr);
  },

  // 改云飘过 变化
  changeCloudType(e) {
    let that = this;
    // 获得 亮度
    that.setData({ cloudType: e.currentTarget.id });
    let cloudType = that.data.cloudType;
    // 获得 频率
    let cloudBright = that.data.cloudBright;
    let cloudF = that.data.cloudF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let cloudStr =
      'FX:' +
      channel +
      ',1,' +
      cloudType +
      ',' +
      cloudBright +
      ',' +
      cloudF +
      '\r\n';
    console.log(cloudStr);

    that.SendTap(cloudStr);
  },
  // 改云飘过 亮度
  changeCloudBright(e) {
    let that = this;
    // 获得 亮度
    let cloudType = that.data.cloudType;
    // 获得 频率
    let cloudBright = e.detail.value;
    let cloudF = that.data.cloudF;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let cloudStr =
      'FX:' +
      channel +
      ',1,' +
      cloudType +
      ',' +
      cloudBright +
      ',' +
      cloudF +
      '\r\n';
    console.log(cloudStr);
    that.setData({ cloudBright: e.detail.value });
    that.SendTap(cloudStr);
  },
  // 改云飘过 频率
  changeCloudF(e) {
    let that = this;
    // 获得 亮度
    let cloudType = that.data.cloudType;
    // 获得 频率
    let cloudBright = that.data.cloudBright;
    let cloudF = e.detail.value;
    let channel = that.data.channel.slice(-1) * 1 - 1;
    let cloudStr =
      'FX:' +
      channel +
      ',1,' +
      cloudType +
      ',' +
      cloudBright +
      ',' +
      cloudF +
      '\r\n';
    console.log(cloudStr);
    that.setData({ cloudF: e.detail.value });
    that.SendTap(cloudStr);
  },

  // 形参为发送的编码
  SendTap: function (message) {
    let that = this;
    if (that.data.connected) {
      var buffer = new ArrayBuffer(message.length);
      var dataView = new Uint8Array(buffer); // 直接转为 8 位无符号整型数组
      for (var i = 0; i < message.length; i++) {
        dataView[i] = message.charCodeAt(i);
      }
      wx.writeBLECharacteristicValue({
        deviceId: that.data.connectedDeviceId,
        serviceId: that.data.serviceId,
        characteristicId: that.data.characteristics[0].uuid,
        value: buffer,
        success: function (res) {
          console.log('发送成功');
        },
      });
    } else {
      wx.showModal({
        title: '提示',
        content: '蓝牙已断开',
        showCancel: false,
        success: function (res) {
          that.setData({
            searching: false,
          });
        },
      });
    }
  },

  // 通过十六进制参数
  //  改亮度
  changebright(e) {
    let that = this;
    // bright 为亮度 0 - 100
    // bright 为number 型

    // console.log(that);

    // 如果
    let bright = e.detail.value;
    console.log(typeof bright);
    // 将 bright 显示出来
    that.setData({ bright: e.detail.value });
    // 转十六进制并补 0 之后的亮度
    let brightHex = that.zeroFillNoH(bright);
    // 嵌进编码里面， 亮度是在第 10 位
    // FA[0] 00[1] 00[2] 04[3] 00[4] 00[5] 00[6] 00[7] 15[8] 18[9] 64[10] 00[11] 00[12] 00[13] 00[14] 00[15] —— demo：表示 CCT 模式下亮度 100；色温 5400
    // 亮度在 第 10 位；色温在第 8-9 位
    // 拼接的编码
    console.log('brightHex: ', brightHex);
    let setHex = 'FA000004000000001518'.concat(brightHex, '0000000000');
    console.log(setHex);
  },

  // 通过十六进制改色温
  changecolorTemp(e) {
    let that = this;
    // 获得色温的十进制数
    let colorTemp = e.detail.value;
    // 转十六进制并补 0 之后的色温
    let colorTempHex = that.zeroFill(colorTemp);
    console.log(colorTemp);
    console.log(colorTempHex);
    // console.log(this.data.bright);
    // console.log(typeof this.data.bright);

    // 拿到同一个功能的 亮度值 bright, 转十六进制并补 0 之后的亮度  brightHex
    let brightHex = that.zeroFillNoH(this.data.bright);
    console.log(brightHex);
    // 拼接编码，色温在第 8 - 9 位，亮度在第 10 位
    // FA[0] 00[1] 00[2] 04[3] 00[4] 00[5] 00[6] 00[7] 15[8] 18[9] 64[10] 00[11] 00[12] 00[13] 00[14] 00[15] —— demo：表示 CCT 模式下亮度 100；色温 5400
    // 亮度在 第 10 位；色温在第 8-9 位
    let setHex = 'FA00000400000000'.concat(
      colorTempHex,
      brightHex,
      '0000000000'
    );
    console.log(setHex);
    this.setData({ colorTemp: e.detail.value });
  },

  // 补零操作
  zeroFill: function (num) {
    let numL, numH;
    if (num < 16) {
      // 高位补两个0，低位补一个 0
      num = num.toString(16);
      // console.log('原16位', num);
      numL = '0' + num.slice(-1);
      numH = '00'.concat(num.slice(0, -2));
      num = numH.concat(numL);
    } else if (num >= 16 && num < 256) {
      // 高位补两个 0，低位不用补 0
      num = num.toString(16);
      // console.log('原始十六位', num);
      numL = num.slice(-2);
      numH = '00'.concat(num.slice(0, -2));
      num = numH.concat(numL);
    } else if (num >= 256 && num < 4096) {
      // 高位补一个 0
      num = num.toString(16);
      // console.log('原始十六位', num);
      numL = num.slice(-2);
      numH = '0'.concat(num.slice(0, -2));
      num = numH.concat(numL);
    } else {
      num = num.toString(16);
      // console.log('原始十六位', num);
      numL = num.slice(-2);
      numH = num.slice(0, -2);
    }
    // console.log(numH);
    // console.log(numL);
    // console.log(num);
    // console.log(typeof num);
    return num;
  },
  // 补零，不需要补高位
  zeroFillNoH: function (num) {
    let numL, numH;
    if (num < 16) {
      // 补一个 0
      num = num.toString(16);
      // console.log('原16位', num);
      numL = '0' + num.slice(-1);
      // numH = '00'.concat(num.slice(0, -2))
      num = numL;
    } else {
      // 不用补零
      num = num.toString(16);
      // console.log('原始十六位', num);
      // numL = num.slice(-2)
      // numH = num.slice(0, -2)
    }
    // console.log(numH);
    // console.log(numL);
    // console.log(num);
    // console.log(typeof num);
    return num;
  },

  // 发送十六进制
  // SendTapHex: function () {
  //   let that = this;
  //   // FA000000000000001518640000000000, FA04050001010506
  //   // 拼接的
  //   that.writeBluetoothData1('FA0000040000000015186400000000FF', 16); // ('十六进制数', 16)
  // },
  // writeBluetoothData1: function (e, num) {
  //   //写入执行1指令 (e, num)
  //   let that = this;
  //   wx.writeBLECharacteristicValue({
  //     deviceId: that.data.connectedDeviceId,
  //     serviceId: that.data.serviceId,
  //     characteristicId: that.data.characteristics[0].uuid,
  //     value: that.getBinaryData1(e, num), // (e, num)
  //     success(res) {
  //       console.log('writeBLECharacteristicValue success', res.errMsg);
  //     },
  //     fail(res) {
  //       console.log('writeBLECharacteristicValue fail', res.errMsg);
  //     },
  //   });
  // },
  // getBinaryData1: function (message, num) {
  //   //将数据转为二进制数组 (message, num)
  //   let buffer = new ArrayBuffer(num); // num
  //   let dataView = new DataView(buffer);
  //   var numTitle = 0;
  //   for (var i = 0; i < message.length; i = i + 2) {
  //     var numStr16 = message.substr(i, 2);
  //     var num1 = parseInt(numStr16, 16);
  //     dataView.setUint8(numTitle, num1);
  //     numTitle++;
  //   }
  //   return buffer;
  // },

  changer(e) {
    this.setData({ r: e.detail.value });
  },
  changeg(e) {
    this.setData({ g: e.detail.value });
  },
  changeb(e) {
    this.setData({ b: e.detail.value });
  },
  changef(e) {
    this.setData({ f: e.detail.value });
  },
  colorChanging(e) {
    let color = e.currentTarget.dataset.color; //获取当前slider组件的data-color值
    //let value = e.detail.value; //获取当前slider组件的value值
    //console.log(color,value) //在console中显示信息
    this.setData({
      [color]: e.detail.value, //将value值赋值给数组color
    });
  },

  swiperChange: function (e) {
    //切换
    if (e.detail.source == 'touch') {
      let curTab = e.detail.current;
      this.setData({
        curTab: e.detail.current,
      });
    }
  },

  // 顶部部光效轮播图部分
  changeSwiper: function (e) {
    this.setData({
      curTab: e.detail.current,
    });
  },

  // 底部光效轮播图部分
  changeSwiper1: function (e) {
    this.setData({
      currentIndex1: e.detail.current,
    });
  },

  // 底部弹窗
  // 点击选项
  getOption: function (e) {
    var that = this;
    that.setData({
      channel: e.currentTarget.dataset.value,
      hideFlag: true,
    });
    let channel = that.data.channel.slice(-1) * 1 - 1;
    console.log(that.data.channel);
    console.log(channel);
  },
  //取消
  mCancel: function () {
    var that = this;
    that.hideModal();
  },

  setChannel: function () {
    var that = this;
    that.setData({
      hideFlag: false,
    });

    // 创建动画实例
    var animation = wx.createAnimation({
      duration: 400, //动画的持续时间
      timingFunction: 'ease', //动画的效果 默认值是linear->匀速，ease->动画以低速开始，然后加快，在结束前变慢
    });
    this.animation = animation; //将animation变量赋值给当前动画
    var time1 = setTimeout(function () {
      that.slideIn(); //调用动画--滑入
      clearTimeout(time1);
      time1 = null;
    }, 100);
  },

  // 隐藏遮罩层
  hideModal: function () {
    var that = this;
    var animation = wx.createAnimation({
      duration: 400, //动画的持续时间 默认400ms
      timingFunction: 'ease', //动画的效果 默认值是linear
    });
    this.animation = animation;
    that.slideDown(); //调用动画--滑出
    var time1 = setTimeout(function () {
      that.setData({
        hideFlag: true,
      });
      clearTimeout(time1);
      time1 = null;
    }, 300); //先执行下滑动画，再隐藏模块
  },
  //动画 -- 滑入
  slideIn: function () {
    this.animation.translateY(0).step(); // 在y轴偏移，然后用step()完成一个动画
    this.setData({
      //动画实例的export方法导出动画数据传递给组件的animation属性
      animationData: this.animation.export(),
    });
  },
  //动画 -- 滑出
  slideDown: function () {
    this.animation.translateY(300).step();
    this.setData({
      animationData: this.animation.export(),
    });
  },

  onLoad: function (e) {
    var that = this;

    // 通信测试
    console.log(e);
    that.setData({
      name: e.name,
      connectedDeviceId: e.connectedDeviceId,
    });
    wx.getBLEDeviceServices({
      deviceId: that.data.connectedDeviceId,
      success: function (res) {
        var all_UUID = res.services;
        var index_uuid = -1;
        var UUID_lenght = all_UUID.length;
        /* 遍历服务数组 */
        for (var index = 0; index < UUID_lenght; index++) {
          var ergodic_UUID = all_UUID[index].uuid; //取出服务里面的UUID
          /* 判断是否是我们需要的00010203-0405-0607-0809-0A0B0C0D1910*/
          // if (ergodic_UUID == '00010203-0405-0607-0809-0A0B0C0D1910')
          // console.log(ergodic_UUID)
          // if (ergodic_UUID == '00001801-0000-1000-8000-00805F9B34FB')
          if (ergodic_UUID == '0000FFE0-0000-1000-8000-00805F9B34FB') {
            index_uuid = index;
          }
          // console.log(ergodic_UUID);
        }
        if (index_uuid == -1) {
          wx.showModal({
            title: '提示',
            content: '非我方的设备',
            showCancel: false,
            success: function (res) {
              that.setData({
                searching: false,
              });
            },
          });
        }
        that.setData({
          serviceId: res.services[index_uuid].uuid,
        });
        wx.getBLEDeviceCharacteristics({
          deviceId: e.connectedDeviceId,
          serviceId: res.services[index_uuid].uuid,
          success: function (res) {
            that.setData({
              characteristics: res.characteristics,
            });
            wx.notifyBLECharacteristicValueChange({
              state: true,
              deviceId: e.connectedDeviceId,
              serviceId: that.data.serviceId,
              characteristicId: that.data.characteristics[0].uuid,
              success: function (res) {
                console.log('启用notify成功');
              },
              fail(res) {
                console.log(res);
              },
            });
          },
        });
      },
    });
    wx.onBLEConnectionStateChange(function (res) {
      console.log(res.connected);
      that.setData({
        connected: res.connected,
      });
    });
    wx.onBLECharacteristicValueChange(function (res) {
      console.log('接收到数据：' + app.buf2string(res.value));
      var time = that.getNowTime();
      that.setData({
        receiveText: that.data.receiveText + time + app.buf2string(res.value),
      });
    });
    //通信测试

    /**
     * 生命周期函数--监听页面加载
     */
    wx.getSystemInfo({
      success: (res) => {
        //转为rpx
        let ch = (750 / res.screenWidth) * res.windowHeight - 400;
        that.setData({
          ch,
        });
      },
    });
    console.log('onloading......');
    // console.log("id is ", e.id)
    // that.setData({
    //   id:e.id
    // })
    // that.getDataPoints(e.id)
    that.data.myintervalid = setInterval(function () {
      that.onShow();
    }, 3000);

    //get storage data
    // var name = 'equipment_name_' + e.id
    // try {
    //   var value = wx.getStorageSync(name)   //本地缓存中异步获取指定 name 的内容
    //   if (value) {
    //     // Do something with return value
    //     that.setData({
    //       equipment_name: value
    //     })
    //   }
    // } catch (e) {
    //   // Do something when catch error
    //   console.log("get stroage data error!")
    // }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  onShow: function (e) {
    var that = this;
  },
  onHide: function () {
    // 页面隐藏
    clearInterval(this.data.myintervalid);
  },

  onUnload: function () {
    // 页面关闭
    clearInterval(this.data.myintervalid);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {},

  //设置项目-

  setEquipment: function () {
    this.setData({
      showModal: true, //打开弹窗
    });
  },
  inputChange: function (e) {
    this.setData({
      input_name: e.detail.value,
    });
  },
  /**
   * 弹出框蒙层截断touchmove事件
   */
  preventTouchMove: function () {},
  /**
   * 隐藏模态对话框
   */
  // hideModal: function () {
  //   this.setData({
  //     showModal: false    //关闭弹窗
  //   });
  // },
  /**
   * 对话框取消按钮点击事件
   */
  onCancel: function () {
    this.hideModal();
  },
  /**
   * 弹窗对话框确认按钮点击事件
   */
  onConfirm: function () {
    var that = this;
    that.hideModal();
    that.setData({
      equipment_name: that.data.input_name,
    });
    var name = 'equipment_name_' + that.data.id;
    try {
      wx.setStorageSync(name, that.data.input_name);
    } catch (e) {
      // Do something when catch error
      console.log('setStorageSync error!');
    }
    console.log('success');
  },

  // LED控制
  switchChange: function (e) {
    console.log(e.detail.value);
  },
});
