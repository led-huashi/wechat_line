var app = getApp()
Page({
    data: {
        // newsList:[
        //     {id:1,title:"Demo场景"},
        // ],
    Demo_title:"Demo场景",
    showModal: false,
    hideFlag: true,//true-隐藏 false-显示
    animationData: {},//
    scene_name:[],
    input_name:"",
    id:0,

    NewScene_Title:"+",
    array:[0],//默认不显示，要显示一个则为0
    inputVal:[],//所有input的内容
    ch:0
},

// 点击选项
getOption:function(e){
    var that = this;
    that.setData({
      value:e.currentTarget.dataset.value,
      // showModal: true ,  //打开弹窗
      hideFlag: true
    })
  },
  //取消
  mCancel: function () {
    var that = this;
    that.hideModal1();
  },

   // ----------------------------------------------------------------------modal
  // 显示遮罩层
  showModal1: function () {
    var that = this;
    that.setData({
      hideFlag: false
    })
    // 创建动画实例
    var animation = wx.createAnimation({
      duration: 400,//动画的持续时间
      timingFunction: 'ease',//动画的效果 默认值是linear->匀速，ease->动画以低速开始，然后加快，在结束前变慢
    })
    this.animation = animation; //将animation变量赋值给当前动画
    var time1 = setTimeout(function () {
      that.slideIn();//调用动画--滑入
      clearTimeout(time1);
      time1 = null;
    }, 100)
  },
  // 隐藏遮罩层
  hideModal1: function () {
    var that = this;
    var animation = wx.createAnimation({
      duration: 400,//动画的持续时间 默认400ms
      timingFunction: 'ease',//动画的效果 默认值是linear
    })
    this.animation = animation
    that.slideDown();//调用动画--滑出
    var time1 = setTimeout(function () {
      that.setData({
        hideFlag: true
      })
      clearTimeout(time1);
      time1 = null;
    }, 220)//先执行下滑动画，再隐藏模块
  },
  //动画 -- 滑入
  slideIn: function () {
  this.animation.translateY(0).step() // 在y轴偏移，然后用step()完成一个动画
  this.setData({
    //动画实例的export方法导出动画数据传递给组件的animation属性
    animationData: this.animation.export()
  })
  },
  //动画 -- 滑出
  slideDown: function () {
    this.animation.translateY(300).step()
    this.setData({
      animationData: this.animation.export(),
    })
  },

//获取input的值
    getInputVal:function(e){
        var nowIdx=e.currentTarget.dataset.idx;//获取当前索引
        var val=e.detail.value;//获取输入的值
        var oldVal=this.data.inputVal;
        oldVal[nowIdx]=val;//修改对应索引值的内容
        this.setData({
            inputVal:oldVal
        })

        // var name = 'inputVal_' + this.data.id
        // try {
        //     wx.setStorageSync(name,this.data.inputVal)  //name为本地缓存中指定的key, this.data.inputVal为需要存储的内容
        //   } catch (e) {
        //     // Do something when catch error
        //     console.log("setStorageSync error!")
        //   }
        //    console.log("success")

    },
//添加input
    addInput:function(){
        var old=this.data.array;
        old.push(1);//这里不管push什么，只要数组长度增加1就行
        this.setData({
            array: old
        })
    },
//删除input
    delInput:function(e){
        var nowidx=e.currentTarget.dataset.idx;//当前索引
        var oldInputVal=this.data.inputVal;//所有的input值
        var oldarr=this.data.array;//循环内容
        oldarr.splice(nowidx,1);    //删除当前索引的内容，这样就能删除view了
        oldInputVal.splice(nowidx,1);//view删除了对应的input值也要删掉
        if (oldarr.length < 0) {
            oldarr = [0]  //如果循环内容长度为0即删完了，必须要留一个默认的。这里oldarr只要是数组并且长度为1，里面的值随便是什么
        }
        this.setData({
            array:oldarr,
            inputVal: oldInputVal,
            hideFlag: true      //关掉底部框
        })
    },

onLoad: function (e) {
    var that = this;
    var name = ''
    var scenes_origin = ''

    console.log("onloading......");
    // setTimeout(function () {
    //   onenet.getAllDeviceStatus()
    // }, 3000);

    // 获取array数组列表
    scenes_origin = that.data.array


    for (var i = 0; i < scenes_origin.length; i++) {
      name = 'scene_name_' + scenes_origin[i].id
      try {
        var value = wx.getStorageSync(name)
        if (value) {
          scenes_origin[i].scene_name = value;
        }
      } catch (e) {
        // Do something when catch error
        console.log("get stroage data error!")
      }
    }

    that.setData({
      devices : scenes_origin
    })
    // console.log("$$$$$$$$$$$$$", that.data.devices)


    // /**
    // * 生命周期函数--监听页面加载
    // */
    //     wx.getSystemInfo({
    //     success: res => {
    //         //转为rpx
    //         let ch = (750 / res.screenWidth) * res.windowHeight - 80;
    //         this.setData({
    //         ch
    //         })
    //     },
    //     })
    
    //     var that = this;
    //     console.log("onloading......");
    //     console.log("id is ", e.id)
    //     that.setData({
    //     id:e.id
    //     })
    // //  that.getDataPoints(e.id)
    //     that.data.myintervalid = setInterval(function () {
    //     that.onShow()
    //     }, 3000)
    
    
    
    //     //get storage data
    //     var name = 'scene_name_' + e.id
        
    //     try {
    //     var value = wx.getStorageSync(name)   //本地缓存中异步获取指定 name 的内容
    //     if (value) {
    //         // Do something with return value
    //         that.setData({
    //         scene_name: value
    //         })
    //     }
    //     } catch (e) {
    //     // Do something when catch error
    //     console.log("get stroage data error!")
    //     }
    },
//设置项目-
set_scene: function () {
    this.setData({
      showModal: true,   //打开弹窗
      hideFlag: true     //隐藏底部弹窗
    })
  },
  inputChange:function(e) {

    var nowIdx=e.currentTarget.dataset.idx;//获取当前索引
    // var val=e.detail.value;//获取输入的值
    var input_name=e.detail.value;//获取输入的值
    // var oldVal=this.data.input_name;
    var oldVal=this.data.inputVal
    oldVal[nowIdx]=input_name;//修改对应索引值的内容

    this.setData({
      input_name: e.detail.value,
      inputVal:oldVal

    })
  },
  /**
   * 弹出框蒙层截断touchmove事件
   */
  preventTouchMove: function () {
  },
  /**
   * 隐藏模态对话框
   */
  hideModal: function () {
    this.setData({
      showModal: false    //关闭弹窗
    });
  },
  /**
   * 对话框取消按钮点击事件
   */
  onCancel: function () {
    this.hideModal();
  },
  /**
   * 弹窗对话框确认按钮点击事件
   */
  onConfirm: function () {
    var that = this
    that.hideModal();
    that.setData({
      scene_name: that.data.input_name
    })
    var name = 'scene_name_' + that.data.id
    try {
      wx.setStorageSync(name, that.data.input_name)
    } catch (e) {
      // Do something when catch error
      console.log("setStorageSync error!")
    }
     console.log("success")
  },
})

