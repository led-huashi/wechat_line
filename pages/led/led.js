var WxAutoImage = require('../../js/wxAutoImageCal.js');
var onenet = require('../../js/onenet.js');
var API_KEY = "eCQJZEKoyVqA5qV4ef3qTH2OZzo="
var app = getApp();

Page({
  data: {
    showModal: false,
    // imgList0: [    //imgList0
    //   '../../image/swiper1.jpg',
    //   '../../image/swiper1.jpg',
    //   '../../image/swiper1.jpg'
    // ],
    //光效底部轮播图图片
    imgList1: [{
      "title":"火光",
      "img": "../../image/fire.png"
    },
      {
        "title":"烟花",
        "img": "../../image/firework.png"
      },
      {
        "title":"云飘过",
        "img": "../../image/cloud.png"
      }
    ],

    ch: 0,
    curTab: 0,
    equipment_name:"",
    input_name:"",
    id:0,
    redSwitchChecked:false,
    greenSwitchChecked:false,
    blueSwitchChecked:false,
    switchFlag:false,
    myintervalid:0,
    bright: 50,       //亮度
    color_temp: 6300,    //色温
    r: 0,
    g: 0,
    b: 255,
    f:1,              //频率
    //光效的可调节参数
    fire_bright:50,
    fire_saturation:50,
    fire_f:1,
    firework_bright:50,
    firewoek_color:50,
    firework_f:1,
    cloud_bright:50,
    cloud_v:50,
    currentIndex1: 0
    
  },
// Tab部分
  changeTab: function(e) {
    let curTab = e.currentTarget.dataset.curtab;
    this.setData({
      curTab
    })
  },

  swiperChange: function(e) {//切换
    if(e.detail.source == 'touch') {
      let curTab = e.detail.current;
      this.setData({
        curTab
      })
    }
  },
//Sliders部分
changefire_bright(e){
  this.setData({ 
    fire_bright: e.detail.value,
  })
},
changefire_saturation(e){
  this.setData({ 
    fire_saturation:e.detail.value,
  })
},
changefire_f(e){
  this.setData({ 
    fire_f:e.detail.value,
  })
},
changefirework_bright(e){
  this.setData({ 
    firework_bright: e.detail.value,
  })
},
changefirework_color(e){
  this.setData({ 
    firework_color:e.detail.value,
  })
},
changefirework_f(e){
  this.setData({ 
    firework_f:e.detail.value,
  })
},
changecloud_bright(e){
  this.setData({ 
    cloud_bright: e.detail.value,
  })
},
changecloud_v(e){
  this.setData({ 
    cloud_v:e.detail.value,
  })
},
  changebright(e) {
    this.setData({ bright: e.detail.value })
  },
  changecolor_temp(e){
    this.setData({ color_temp: e.detail.value})
  },
  changer(e){
    this.setData({ r: e.detail.value})
  },
  changeg(e){
    this.setData({ g: e.detail.value})
  },
  changeb(e){
    this.setData({ b: e.detail.value})
  },
  changef(e){
    this.setData({ f: e.detail.value})
  },
  colorChanging(e) { 
    let color = e.currentTarget.dataset.color //获取当前slider组件的data-color值
    let value = e.detail.value; //获取当前slider组件的value值
    console.log(color,value) //在console中显示信息
    this.setData({
      [color]: value //将value值赋值给数组color
    })
  },

  // 底部光效轮播图部分
  changeSwiper1: function (e) {
    this.setData({
      currentIndex1: e.detail.current
    })
  },

  onLoad: function (e) {

   /**
   * 生命周期函数--监听页面加载
   */
    wx.getSystemInfo({
      success: res => {
        //转为rpx
        let ch = (750 / res.screenWidth) * res.windowHeight - 80;
        this.setData({
          ch
        })
      },
    })

    var that = this;
    console.log("onloading......");
    console.log("id is ", e.id)
    that.setData({
      id:e.id
    })
    that.getDataPoints(e.id)
    that.data.myintervalid = setInterval(function () {
      that.onShow()
    }, 3000)



    //get storage data
    var name = 'equipment_name_' + e.id
    try {
      var value = wx.getStorageSync(name)   //本地缓存中异步获取指定 name 的内容
      if (value) {
        // Do something with return value
        that.setData({
          equipment_name: value
        })
      }
    } catch (e) {
      // Do something when catch error
      console.log("get stroage data error!")
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  onShow: function (e) {
    var that = this
    // var dat = onenet.getDeviceStatus(e.id);
    // if (dat)
    //   console.log("connect to cloud success!");
    // else
    //   console.log("connect to cloud error!");

    that.getDataPoints(that.data.id)
  },
  onHide: function () {
    // 页面隐藏
    clearInterval(this.data.myintervalid);
  },

  onUnload: function () {
    // 页面关闭
    clearInterval(this.data.myintervalid);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

    /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  btn_connect_fun: function (e) {
    console.log("ready to connect to onenet!");
    var dat = onenet.getDeviceStatus("532808382")
    console.log(dat)
    if (dat)
      console.log("connect to cloud success!");
    else
      console.log("connect to cloud error!");

  },
  btn_red_led_fun: function (e) {
    var that = this;
    if (false == e.detail.value)
    {
      console.log("ready to open red led!");
      onenet.sendCmd(that.data.id, "0")
      that.setData({
        redSwitchChecked: false,
        greenSwitchChecked: false,
        blueSwitchChecked: false
      })
    }else
    {
      console.log("ready to close red led!");
      onenet.sendCmd(that.data.id, "2")
      that.setData({
        redSwitchChecked: true,
        greenSwitchChecked: false,
        blueSwitchChecked: false,
        switchFlag:true
      })
    }
  },
  btn_green_led_fun: function (e) {
    var that = this;
    if (false == e.detail.value) {
      console.log("ready to open blue led!");
      onenet.sendCmd(that.data.id, "0")
      that.setData({
        redSwitchChecked: false,
        greenSwitchChecked: false,
        blueSwitchChecked: false
      })
    } else {
      console.log("ready to close blue led!");
      onenet.sendCmd(that.data.id, "3")
      that.setData({
        redSwitchChecked: false,
        greenSwitchChecked: true,
        blueSwitchChecked: false,
        switchFlag: true
      })
    }
  },
  btn_blue_led_fun: function (e) {
    var that = this;
    if (false == e.detail.value) {
      console.log("ready to open blue led!");
      onenet.sendCmd(that.data.id, "0")
      that.setData({
        redSwitchChecked: false,
        greenSwitchChecked: false,
        blueSwitchChecked: false
      })
    } else {
      console.log("ready to close blue led!");
      onenet.sendCmd(that.data.id, "4")
      that.setData({
        redSwitchChecked: false,
        greenSwitchChecked: false,
        blueSwitchChecked: true,
        switchFlag: true
      })
    }
  },
//显示遮罩

//设置项目-
  set_equipment: function () {
    this.setData({
      showModal: true   //打开弹窗
    })
  },
  inputChange:function(e) {
    this.setData({
      input_name: e.detail.value
    })
  },
  /**
   * 弹出框蒙层截断touchmove事件
   */
  preventTouchMove: function () {
  },
  /**
   * 隐藏模态对话框
   */
  hideModal: function () {
    this.setData({
      showModal: false    //关闭弹窗
    });
  },
  /**
   * 对话框取消按钮点击事件
   */
  onCancel: function () {
    this.hideModal();
  },
  /**
   * 弹窗对话框确认按钮点击事件
   */
  onConfirm: function () {
    var that = this
    that.hideModal();
    that.setData({
      equipment_name: that.data.input_name
    })
    var name = 'equipment_name_' + that.data.id
    try {
      wx.setStorageSync(name, that.data.input_name)
    } catch (e) {
      // Do something when catch error
      console.log("setStorageSync error!")
    }
     console.log("success")
  },

  // LED控制
  switchChange: function (e) {
    console.log(e.detail.value)
  },

  //onenet interfce
  getDataPoints: function (id) {
    var that = this
    var deviceConnected
    var color_value = 0
    //查看设备连接状态，并刷新按钮状态
    wx.request({
      url: "http://api.heclouds.com/devices/" + id + "/datapoints?datastream_id=color",
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        "api-key": API_KEY
      },
      data: {

      },
      success(res) {
        console.log(res)
        deviceConnected = true

        if (that.data.switchFlag != true)
        {
          color_value = res.data.data.datastreams[0].datapoints[0].value
          switch (parseInt(color_value)) {
            case 1:
              that.setData({
                redSwitchChecked: false,
                greenSwitchChecked: false,
                blueSwitchChecked: false
              })
              break;
            case 2:
              that.setData({
                redSwitchChecked: true,
                greenSwitchChecked: false,
                blueSwitchChecked: false
              })
              break;
            case 3:
              that.setData({
                redSwitchChecked: false,
                greenSwitchChecked: true,
                blueSwitchChecked: false
              })
              break;
            case 4:
              that.setData({
                redSwitchChecked: false,
                greenSwitchChecked: false,
                blueSwitchChecked: true
              })
              console.log("color_value is ", color_value)
              console.log("blueSwitchChecked is ", that.data.blueSwitchChecked)
              break;
          }
          console.log("color_value is ", color_value)
        }else{
          that.setData({
            switchFlag:false
          })
        }
      },
      fail(res) {
        console.log("请求失败")
        deviceConnected = false
      }
    })
  },

})