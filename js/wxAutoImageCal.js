// function wxAutoImageCal(e) {
//     console.log(e);
//     var originalWidth = e.detail.width;
//     var originalHeight = e.detail.height;
//     var windowWidth = 0;
//     var windowHeight = 0;
//     var autoWidth = 0;
//     var autoHeight = 0;
//     var results = {};
//     wx.getSystemInfo({
//       success: function(res) {
//         // success
//         var winWidth = res.windowWidth;
//         var winHeight = res.windowHeight;
//         if(originalWidth > winWidth){
//             autoWidth = winWidth;
//             autoHeight = (autoWidth * originalHeight) / originalWidth;
//             results.imageWidth = autoWidth + 'px';
//             results.imageHeight = autoHeight + 'px';
//             console.log(results.imageHeight,results.imageWidth);
//         }
//         else{
//             results.imageWidth = originalWidth + 'px';
//             results.imageHeight = originalHeight + 'px';
//             console.log(results.imageHeight,results.imageWidth);
//         }
//       }
//     })
//     return results;
// }
// module.exports.wxAutoImageCal = wxAutoImageCal

//计算视觉优先的图片宽高
function wxAutoImageCal(originalWidth, originalHeight, that, bindName, temImage) {

  var arr = temImage.attr.style;
  var widthIndex = arr.indexOf("width:");

  console.log(widthIndex);

  var widthValue = '';
  if (widthIndex != -1) {
    // widthValue = arr[widthIndex + 1];

    console.log(arr);
    var trr = arr.split(";");///sophie
    for (let i = 0; i < trr.length; ++i) {
      if (trr[i].indexOf("width") != -1) {
        widthValue = trr[i].split(":")[1];
      }
    }
    // console.log(trr);
    console.log(widthValue);
  }
  var percentageIndex = widthValue.search("%");
  var pixelIndex = widthValue.search("px");
  var percentageWidthValue = '';
  var pixelWidthValue = '';
  var pixelHeightValue = '';
  console.log(percentageIndex);
  console.log(pixelIndex);
  /**
   * 获取width的百分比数值
   * 因为widthValue是带有%和;的，例如宽度为50%，那么widthValue的数据格式为widthValue == "50%;"，
   * 因此多出来后面两个字符'%;'，所以要去除后面两位
   */
  if ((percentageIndex > 0) && (widthValue.length == percentageIndex + 2)) {
    percentageWidthValue = widthValue.slice(0, -2);
  }

  /**
   * 获取width的px数值
   * 因为widthValue是带有px和;的，例如宽度为50px，那么widthValue的数据格式为widthValue == "50px;"，
   * 因此多出来后面三个字符'px;'，所以要去除后面三位，
   * 而当width为px显示时，height和width是成对出现的
   */
  if ((pixelIndex > 0) && (widthValue.length == pixelIndex + 2)) {
    pixelWidthValue = widthValue.slice(0, -2);

    var heightIndex = arr.indexOf("height:");
    var heightValue = '';
    if (heightIndex != -1) {
      // heightValue = arr[heightIndex + 1];
      var hrr = arr.split(";");///sophie
      for (let i = 0; i < hrr.length; ++i) {
        if (hrr[i].indexOf("height") != -1) {
          heightValue = hrr[i].split(":")[1];
        }
      }
      console.log(heightValue);
    }
    var pixelHeightIndex = heightValue.search("px");
    if ((pixelHeightIndex > 0) && (heightValue.length == pixelHeightIndex + 2)) {
      pixelHeightValue = heightValue.slice(0, -2);
    }
  }
  console.log(pixelHeightValue);
  //获取图片的原始长宽
  var windowWidth = 0, windowHeight = 0;
  var autoWidth = 0, autoHeight = 0;
  var results = {};
  var padding = that.data[bindName].view.imagePadding;
  windowWidth = realWindowWidth - 2 * padding;
  windowHeight = realWindowHeight;

  /**
   * 1、如果图片的宽度style是百分比的参数形式，那么图片在微信中展示的宽度就定义为 手机屏幕宽度*宽度百分比；
   * 2、如果图片的宽度style是px的参数形式，并且该宽度小于屏幕宽度，那么图片在微信中展示的宽、高就定义为 style所设置的宽、高；
   * 3、此外，则按原插件逻辑进行图片大小定义，在图片width大于手机屏幕width时等比例缩放至屏幕大小，
   *   未大于手机屏幕width时则按图片原尺寸显示
   */
  if (percentageWidthValue) {
    autoWidth = (windowWidth * percentageWidthValue) / 100;
    autoHeight = (autoWidth * originalHeight) / originalWidth;
    results.imageWidth = autoWidth;
    results.imageheight = autoHeight;

  } else if (pixelWidthValue && pixelHeightValue && (pixelWidthValue <= windowWidth)) {
    results.imageWidth = pixelWidthValue;
    results.imageheight = pixelHeightValue;
  } else {
    //判断按照那种方式进行缩放
    if (originalWidth > windowWidth) {//在图片width大于手机屏幕width时候
      autoWidth = windowWidth;

      autoHeight = (autoWidth * originalHeight) / originalWidth;
      results.imageWidth = autoWidth;
      results.imageheight = autoHeight;

    } else {
      results.imageWidth = originalWidth;
      results.imageheight = originalHeight;
    }
  }
  return results;

}
